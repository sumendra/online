<?php 
 class belajar extends CI_Controller {
     public function __construct(){
         parent:: __construct();
         $this->load->model('belajar_model');
     }
     public function index () {
         $belajar['title']="this belajar";
         $belajar['belajar_awal']=$this->belajar_model->lihat_belajar();
         $this->load->view('template/header');
         $this->load->view('Belajar/index', $belajar);
         $this->load->view('template/footer');
     }
 }