<?php  
    class post extends CI_Controller {
        public function __construct() {
            parent:: __construct();
            $this->load->model('post_model');
           // $this->load->model('home_model');
        }
        public function index (){
            $post_komentar['post_aery']=$this->post_model->post_lihat();
            $post_komentar['search_post']=$this->post_model->post_search();
            $this->load->view('template/header');
            $this->load->view('post/index', $post_komentar );
            $this->load->view('template/footer');
        }
        
        public function post($id_post){
			$post_halamanutama['post_quer']=$this->post_model->post_model($id_post);
			$post_halamanutama['komentar_amat']=$this->post_model->lihat_komentar($id_post);
			$post_halamanutama['komentar_semua']=$this->post_model->lihat_username_komentar($id_post);
			$this->post_model->update_view($id_post);
			$this->load->view('template/header');
			$this->load->view('post/post',$post_halamanutama);
			$this->load->view('template/footer');
		}
		
    }
?>