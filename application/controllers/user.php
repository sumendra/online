<?php 
    class user extends CI_Controller{
        public function __construct(){
            parent:: __construct();
            $this->load->model('login_model');
        }
        public function login(){
            //$data['title']='this is signin';
			$this->load->helper('security');
			
			$this->form_validation->set_rules('username','username','required|callback_check_username_exists');
            $this->form_validation->set_rules('password','password','required|callback_check_password_exists');
            $username=$this->input->post('username');
            $password=$this->input->post('password');
            $email=$this->input->post('email');
            $where=array (
                'username' => $username,
                'password' => $password,
                'username' => $email,
            );
			if($this->form_validation->run()===false){
			$this->load->view('user/login');
			}
			else{
               // $cek = $this->login_model->cek_login();
                    $data_session =array(
                        'nama' =>$username,
                        'status'=>"login",
                    );
                    $this->login_model->insert_login($username);
                    $this->session->set_userdata($data_session);
                    redirect(base_url("halamanutama"));
                
			}
        }

        public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists','pilih username lainnnya');
			if($this->login_model->check_username_exists($username)){
				return true;
			}
			else{
				return false;
			}
		}
		public function check_password_exists($password){
			$this->form_validation->set_message('check_password_exists','pilih password lainnya');
			if($this->login_model->check_password_exists($password)){
				return true;
			}
			else{
				
				return false;
			}
        }
        public function check_email_exists ($email) {
            $this->form_validation->set_message('check_email_exists','pilih email lainnya ');
            if($this->login_model->check_email_exists($email)){
                return true;
            }
            else{
                return false;
            }
        }
            
        function logout(){
            $this->session->sess_destroy();
            // redirect controller login in controller 
            redirect(base_url());
        }

        function registrasi_premium(){
            $this->load->view('template/header');
            $this->load->view('user/registerpremium');
            $this->load->view('template/footer');
        }
        function cancellogin(){
            echo"<script>window.location=history.go(-2)</script>";
        }
        
        function cancelregistrasi () {
            echo"<script>window.location=history.go(-2)</script>";
        }
        function registrasi_profesional (){
            $this->form_validation->set_rules('email','email','required|callback_check_email_exists');
            $this->form_validation->set_rules('username','username','required');
            $this->form_validation->set_rules('password','password','required');

            if($this->form_validation->run()==false){
                $this->load->view('user/registrasiprofesional');
                $this->load->view('template/footer');
            }
            else{ 
                    
            //      $this->load->view('template/header');
                    $this->load->view('user/registrasiprofesional');
                    $this->load->view('template/footer');
                    $this->login_model->insert_registrasiprofesional();
                    $this->login_model->insert_kelas();
                   // if($this->login_model->insert_kelas()==true){
                       echo"<script>alert('berhasil registrasi');</script>";
                        redirect(base_url('user/login'));
                   // }
                }
           
        }
        function registrasi_basic (){
            $this->form_validation->set_rules('email','email','required|callback_check_email_exists');
            $this->form_validation->set_rules('username','username','required');
            $this->form_validation->set_rules('password','password','required');

            if($this->form_validation->run()==false){
                $this->load->view('user/registrasibasic');
                $this->load->view('template/footer');
            }
            else{ 
                    $this->login_model->insert_registrasibasic();
                    $this->login_model->insert_kelasbasic();
            //      $this->load->view('template/header');
                    $this->load->view('user/registrasibasic');
                    $this->load->view('template/footer');
                    redirect(base_url('user/login'));
                }
           
        }
        function registrasi_teamlearn (){
            $this->form_validation->set_rules('email','email','required|callback_check_email_exists');
            $this->form_validation->set_rules('username','username','required');
            $this->form_validation->set_rules('password','password','required');

            if($this->form_validation->run()==false){
                $this->load->view('user/registrasiteamlearn');
                $this->load->view('template/footer');
            }
            else{ 
                    $this->login_model->insert_registrasiteamlearn();
                    $this->login_model->insert_kelasteamlearn();
            //      $this->load->view('template/header');
                    $this->load->view('user/registrasiteamlearn');
                    $this->load->view('template/footer');
                    redirect(base_url('user/login'));
                }
           
        }
        function registrasi_expert (){
            $this->form_validation->set_rules('email','email','required|callback_check_email_exists');
            $this->form_validation->set_rules('username','username','required');
            $this->form_validation->set_rules('password','password','required');

            if($this->form_validation->run()==false){
                $this->load->view('user/registrasiexpert');
                $this->load->view('template/footer');
            }
            else{ 
                    $this->login_model->insert_registrasiexpert();
                    $this->login_model->insert_kelasexpert();
            //      $this->load->view('template/header');
                    $this->load->view('user/registrasiexpert');
                    $this->load->view('template/footer');
                    redirect(base_url('user/login'));
                }
           
        }

    }

