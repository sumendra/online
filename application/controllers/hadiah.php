<?php  
    class hadiah extends CI_Controller {
        public function __construct() {
            parent:: __construct();
                $this->load->model('hadiah_model');
        }
        public function index () {
            $hadiah_awal['hadiah_lihat']=$this->hadiah_model->lihat_hadiah();
            $this->load->view('template/header');
            $this->load->view('hadiah/index', $hadiah_awal);
            $this->load->view('template/footer');
        }
    }
?>