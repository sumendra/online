<?php  
    class halamanutama extends CI_Controller {
        public function __construct () {
            parent:: __construct();
            $this->load->model('home_model');     
        }
        public function index (){
            //$index_baru['lomba_awal']=$this->lomba_model->lihat_lomba();
            $index_baru['temp']=$this->home_model->lihat_post();
            $index_baru['terbaru']=$this->home_model->lihat_terbaru();
            
            $this->form_validation->set_rules('username','username','required|callback_check_username_exists');
            $this->form_validation->set_rules('password','password','required|callback_check_password_exists');
            $this->form_validation->set_rules('email','email','required|callback_check_email_exists|valid_email');
            if($this->form_validation->run()== false){
                $this->load->view('template/header');
                $this->load->view('home/index', $index_baru);
                $this->load->view('template/footer');
            }
            else{
                $this->home_model->register_user();
                $this->load->view('template/header');
                $this->load->view('home/index',$index_baru);
                $this->load->view('template/footer');
            }

        }

        public function check_username_exists($username){
			$this->form_validation->set_message('check_username_exists','pilih username lainnnya');
			if($this->login_model->check_username_exists($username)){
				return true;
			}
			else{
				return false;
			}
		}
		public function check_password_exists($password){
			$this->form_validation->set_message('check_password_exists','pilih password lainnya');
			if($this->login_model->check_password_exists($password)){
				return true;
			}
			else{
				
				return false;
			}
        }
        public function check_email_exists ($email) {
            $this->form_validation->set_message('check_email_exists','pilih email lainnya ');
            if($this->login_model->check_email_exists($email)){
                return true;
            }
            else{
                return false;
            }
        }

        public function sesudahlogin () {
            $index_login['semua_sofa']=$this->home_model->tampil_basah();
            $index_login['sepatu_dalam']=$this->home_model->masuk_sepatu();
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin', $index_login);
            $this->load->view('home/indexlogin',$index_login);
            $this->load->view('template/footer');
        }

        public function post($id_post){
            $index_login['post_quer']=$this->home_model->post_model($id_post);
            $index_login['komentar_amat']=$this->home_model->lihat_komentar($id_post);
            $index_login['komentar_semua']=$this->home_model->lihat_username_komentar($id_post);
            $index_login['template_member']=$this->home_model->template_username();
            $this->home_model->update_view($id_post);
            $this->load->view('template/headerlogin', $index_login);
            $this->load->view('post/post_login',$index_login);
            $this->load->view('template/footer');
        } 

        public function ceria_post($id_post){
            $this->home_model->update_like($id_post);
            echo"<script>window.location=history.go(-2)</script>";
        }

        public function post_komentar() {
            $this->form_validation->set_rules('isi','isi','required');
            if($this->form_validation->run()== false) {
                echo"<script>window.location=history.go(-1)</script>";
            }else{
                $this->home_model->masukan_komentar();
                echo"<script>window.location=history.go(-1)</script>";
            }
        }

        public function belajarindex() {
           
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' , $index_login);
            $this->load->view('belajar/indexlogin',$index_login);
            $this->load->view('template/footer');
        }

        public function fetch () {
            $query='';
            if($this->input->post('query')){
                $query=$this->input->post('query');
            }
            $index_login['belajar_awal']=$this->home_model->lihat_belajar($query);
           // $index_login ['hasil_ajax'] = $this->home_model->fetch_data($query);
            $this->load->view('belajar/search_belajar', $index_login);
            
        }


        public function belajar ($id_belajar) {
            $index_login['belajarsatu']=$this->home_model->belajar_modul($id_belajar);
            $index_login['template_member']=$this->home_model->template_username();
            $this->home_model->update_poin($id_belajar);
           // $this->home_model->update_view($id_post);

            $this->load->view('template/headerlogin');
            $this->load->view('belajar/belajar',$index_login);
            $this->load->view('template/footer');
        }

        public function seminarindex () {
            $index_login['seminar_awal']=$this->home_model->lihat_seminar();
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' , $index_login);
            $this->load->view('seminar/indexlogin',$index_login);
            $this->load->view('template/footer');
        }

        public function seminar ($id_seminar) {
            $index_login['seminarsatu']=$this->home_model->seminar_modul($id_seminar);
            $index_login['template_member']=$this->home_model->template_username();
           // $this->home_model->seminar_poin($id_seminar);
           // $this->home_model->update_view($id_post);

            $this->load->view('template/headerlogin', $index_login);
            $this->load->view('seminar/seminar',$index_login);
            $this->load->view('template/footer');
        }

        public function lombaindex () {
            $index_login['lomba_awal']=$this->home_model->lihat_lomba();
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' , $index_login);
            $this->load->view('lomba/indexlogin',$index_login);
            $this->load->view('template/footer');
        }

        public function lomba ($id_lomba) {
            $lomba_login['lombasatu']=$this->home_model->lomba_modul($id_lomba);
            $index_login['template_member']=$this->home_model->template_username();
            //$this->home_model->lomba_poin($id_lomba);
           // $this->home_model->update_view($id_post);
           
            $this->load->view('template/headerlogin' ,$index_login);
            $this->load->view('lomba/lomba',$lomba_login);
            $this->load->view('template/footer');
        }

        public function posthome () {
            $index_login['post_login']=$this->home_model->post_login();
            $index_login['search_post']=$this->home_model->post_search();
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' ,$index_login);
            $this->load->view('post/indexlogin',$index_login);
            $this->load->view('template/footer');
        }
        public function registrasiseminar ($id_seminar) {
            $index_login['registrasi_tampil']=$this->home_model->tampil_registrasi($id_seminar);
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin',$index_login);
            $this->load->view('seminar/registrasiseminar', $index_login);
            $this->load->view('template/footer');
        }

        public function seminarseminar () {
           // $this->form_validation->set_rules('nama_lengkap','nama_lengkap','required');
            $this->form_validation->set_rules('kota_asal','kota_asal','required');
            $this->form_validation->set_rules('no_telp','no_telp','required');
            $this->form_validation->set_rules('manfaat','manfaat','required');
            $this->form_validation->set_rules('tujuan','tujuan','required');

            if($this->form_validation->run() === false){
                echo"<script>window.location=history.go(-1)</script>";
            }
            else{
                $this->home_model->seminar_poin();
                echo"<script>window.location=history.go(-2)</script>";
            }
        }

        public function registrasibatu ($id_lomba) {
            $index_login['registrasi_tampil']=$this->home_model->lomba_template($id_lomba);
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' , $index_login);
            $this->load->view('lomba/registrasilomba', $index_login);
            $this->load->view('template/footer');
        }

        public function lombalomba () {
            // $this->form_validation->set_rules('nama_lengkap','nama_lengkap','required');
             $this->form_validation->set_rules('kota_asal','kota_asal','required');
             $this->form_validation->set_rules('no_telp','no_telp','required');
             $this->form_validation->set_rules('manfaat','manfaat','required');
             $this->form_validation->set_rules('tujuan','tujuan','required');
 
             if($this->form_validation->run() === false){
                 echo"<script>window.location=history.go(-1)</script>";
             }
             else{
                 $this->home_model->lomba_xp();
                 echo"<script>window.location=history.go(-2)</script>";
             }
         }

         public function hadiah () {
            $halaman_hadiah['hadiah_lihat']=$this->home_model->lihat_hadiah();
            $halaman_hadiah['hadiah_session']=$this->home_model->hadiahsuka();
            $halaman_hadiah['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' ,$halaman_hadiah);
            $this->load->view('hadiah/indexlogin', $halaman_hadiah);
            $this->load->view('template/footer');
         }

         public function hadiahbaru ($id_hadiah) {
             $index_login['registrasi_member']=$this->home_model->member_hadiah($id_hadiah);
             $index_login['registrasi_tampil']=$this->home_model->hadiah_template($id_hadiah);
             $index_login['template_member']=$this->home_model->template_username();
             if(!empty($index_login['registrasi_member'])){
                 foreach($index_login['registrasi_member'] as $rowp){
                    if(!empty($index_login['registrasi_tampil'])){
                        foreach($index_login['registrasi_tampil'] as $rowb ) {
                            if($rowp->poin > $rowb->poin_dibutuhkan){
                                $this->load->view('template/headerlogin');
                                $this->load->view('hadiah/registrasihadiah', $index_login);
                                $this->load->view('template/footer');
                            }
                            else{
                                echo"<script>alert('poin kurang')</script>";
                                echo"<script>window.location=history.go(-1)</script>";
                            }
                        }
                    }
                 }
             }  
        }

        public function hadiahhadiah () {
            // $this->form_validation->set_rules('nama_lengkap','nama_lengkap','required');
             $this->form_validation->set_rules('kota_asal','kota_asal','required');
             $this->form_validation->set_rules('no_telp','no_telp','required');
 
             if($this->form_validation->run() === false){
                 echo"<script>window.location=history.go(-1)</script>";
             }
             else{
                 $this->home_model->hadiah_xp();
                 echo"<script>window.location=history.go(-2)</script>";
             }
         }

         public function profil () {
            $index_login['halamanmasuk']=$this->home_model->profil_member();
            $index_login['belanja_poin']=$this->home_model->profil_learning();
            $index_login['seminar_poin']=$this->home_model->profil_seminar();
            $index_login['lomba_poin']=$this->home_model->profil_lomba();
            $index_login['template_member']=$this->home_model->template_username();
            $this->load->view('template/headerlogin' , $index_login);
            $this->load->view('profil/profil' , $index_login);
            $this->load->view('template/footer');
         }

         public function profil_edit ($id_member) {
             $index_profil['profil_status']=$this->home_model->profil_hadiah($id_member);
            // $this->load->view('template/headerlogin');
             $this->load->view('profil/edit' , $index_profil);
            // $this->load->view('template/footer');
         }

         public function cancel_profil () {
            echo"<script>window.location=history.go(-2)</script>";
         }

         public function tutorial_profil () {
            $this->form_validation->set_rules('username','username','required');
            //$this->form_validation->set_rules('upload','upload','required');
            $this->form_validation->set_rules('email','email','required');
            $this->form_validation->set_rules('password','password','required');
            if($this->form_validation->run() === false){
                        echo"<script>alert('gagal')</script>";
                         echo"<script>window.location.go(-2)</script>";
            }
            else{
                $data['pic_file']=$this->input->post('upload');
                $config['upload_path'] = APPPATH.'../Asset/unggah/';
                $config['allowed_types']='gif|jpg|png';
                $config['max_size']=1000;
                $this->load->library('upload',$config);
                if(!$this->upload->do_upload('upload')){
                    $error = array ('error'=>$this->upload->display_errors());
                    $this->load->view('profil/edit', $error);
                    echo"<script>alert('error')</script>";
                }
                else{
                    $upload_data=$this->upload->data();
                    $data['upload']=$upload_data['file_name'];
                    $this->home_model->rumah_profil($data);
                    echo"<script>alert('berhasil')</script>";
                     echo"<script>window.location=history.go(-2)</script>";
                }
            }
         }

    }
?>