<?php  
    class lomba extends CI_Controller {
        public function __construct () {
            parent:: __construct();
            $this->load->model('lomba_model');     
        }
        public  function index (){
            $lomba_baru['lomba_awal']=$this->lomba_model->lihat_lomba();

            $this->load->view('template/header');
            $this->load->view('lomba/index', $lomba_baru);
            $this->load->view('template/footer');

        }
    }
?>