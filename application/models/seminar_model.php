<?php  
    class seminar_model extends CI_Model {
        public function __construct () {
            parent:: __construct();
            $this->load->database();
        }

        public function lihat_seminar () {
            $this->db->select("*");
            $this->db->from("seminar");
            $this->db->order_by("id_seminar","DESK");
            $this->db->limit(4);
            $cuey=$this->db->get();
            $seminar_awal=$cuey->result();
            return $seminar_awal;
        }
    }
?>