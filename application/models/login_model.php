<?php 
 class login_model extends CI_Model {
     public function __constructor(){
         parent::__constructor;
         $this->load->database();
     } 
     
     public function check_username_exists($username){
        /*jika username = username tidak ada */$query=$this->db->get_where('member', array('username'=>$username));
        if(empty($query->row_array())){
            return false;
        /*jika ada username maka dia akan melanjutkan proses */}
        else{
            return true;
        }
    }
    public function check_password_exists($password){
        $query=$this->db->get_where('member', array('password'=>$password));
        if(empty($query->row_array())){
            return false;
        }
        else{
            return true;
        }
    }
    public function insert_login($username){
        $qury=$this->db->get_where('member',array('username'=>$username));
        foreach ($qury->result()as $row){

            $insertlogin = array (
                'id_member'=>$row->id_member,
            );
            $this->db->insert('login',$insertlogin);
        }
    }
    public function insert_registrasiprofesional () {
        $profesional="profesional";
        $user_profesional="user.png";
        $registrasiprofesional= array (
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>$this->input->post('password'),
            'status'=>$profesional,
            'gambar'=>$user_profesional,
        );
        $this->db->insert('member',$registrasiprofesional);
        $id_member=$this->db->insert_id();
        return $id_member;

    }
    public function check_email_exists ($email) {
        $quer=$this->db->get_where('member',array('email'=>$email));
        if(empty($quer->row_array())){
                return true;
        }
        else{
            return false;
        }
    }
    public function insert_kelas () {
        $kelas_kedalam= array (
            'nama_kelas'=> $this->input->post('pilih_kelas'),
            'status_pembayaran'=>"belum",
            'id_member'=>$this->insert_registrasiprofesional(),
        );
        $this->db->insert('kelas_userpremium', $kelas_kedalam);
    }
    public function insert_registrasibasic () {
        $basic="basic";
        $user_basic="user.png";
        $registrasibasic= array (
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>$this->input->post('password'),
            'status'=>$basic,
            'gambar'=>$user_basic,
        );
        $this->db->insert('member',$registrasibasic);
        $id_member=$this->db->insert_id();
        return $id_member;

    } 
    public function insert_kelasbasic () {
        $kelas_kedalam= array (
            'nama_kelas'=> $this->input->post('pilih_kelas'),
            'status_pembayaran'=>"belum",
            'id_member'=>$this->insert_registrasibasic(),
        );
        $this->db->insert('kelas_userpremium', $kelas_kedalam);
    }	
    public function insert_registrasiteamlearn () {
        $teamlearn="teamlearn";
        $user_teamlearn="user.png";
        $registrasiteamlearn= array (
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>$this->input->post('password'),
            'status'=>$teamlearn,
            'gambar'=>user.png,
        );
        $this->db->insert('member',$registrasiteamlearn);
        $id_member=$this->db->insert_id();
        return $id_member;

    }	
    public function insert_kelasteamlearn () {
        $kelas_kedalam= array (
            'nama_kelas'=> $this->input->post('pilih_kelas'),
            'status_pembayaran'=>"belum",
            'id_member'=>$this->insert_registrasiteamlearn(),
        );
        $this->db->insert('kelas_userpremium', $kelas_kedalam);
    }	
    public function insert_registrasiexpert () {
        $expert="expert";
        $user_expert="user.png";
        $registrasiexpert= array (
            'email'=>$this->input->post('email'),
            'username'=>$this->input->post('username'),
            'password'=>$this->input->post('password'),
            'status'=>$expert,
            'gambar'=>$user_expert,
        );
        $this->db->insert('member',$registrasiexpert);
        $id_member=$this->db->insert_id();
        return $id_member;

    }	
    public function insert_kelasexpert () {
        $kelas_kedalam= array (
            'nama_kelas'=> $this->input->post('pilih_kelas'),
            'status_pembayaran'=>"belum",
            'id_member'=>$this->insert_registrasiexpert(),
        );
        $this->db->insert('kelas_userpremium', $kelas_kedalam);
    }	
 }