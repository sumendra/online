<?php  
	class home_model extends CI_Model {
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}
		public function register_user(){
			$free="free";
			$indexlogin = array (
				'email'=>$this->input->post('email'),
				'username'=>$this->input->post('username'),
				'password'=>$this->input->post('password'),
				'status'=>$free,
				'poin'=>0,
				'xp'=>0,
				'gambar'=>user.png,
			);
			$this->db->insert('member',$indexlogin);
			$id_register=$this->db->insert_id();
			$insert_free= array (
				'id_member'=>$id_register
			);
			$this->db->insert('kelas_userfree', $insert_free);
		}
		public function check_username_exists($username){
			$query=$this->db->get_where('member',array('username'=>$username));
			if(empty($query->row_array())){
				return true;
			}
			else{
				return false;
			}
		}
		public function check_email_exists($email){
			$query=$this->db->get_where('member',array('email'=>$email));
			if(empty($query->row_array())){
				return true;
			}
			else{
				return false;
			}
		}
		public function check_password_exists($password){
			$query=$this->db->get_where('member',array('password'=>$password));
			if(empty($query->row_array())){
				return true;
			}
			else{
				return false;
			}
		}
		public function lihat_post(){
			$this->db->select('*');
			$this->db->from('post');
			$this->db->order_by('suka','Desk');
			$this->db->limit(6);
			$qury = $this->db->get();		
		
				$temp=$qury->result();
				return $temp;
						
		}
		public function lihat_terbaru(){
			$this->db->select('*');
			$this->db->from('post');
			$this->db->order_by('id_post','Desk');
			$this->db->limit(6);
			$qucy=$this->db->get();
			
				$terbaru=$qucy->result();
				return $terbaru;
			
		}
		
		public function tampil_basah() {
			$this->db->select("*");
			$this->db->from("post");
			$this->db->order_by('view','DESK');
			$this->db->limit(6);
			$eury=$this->db->get();
			$semua_sofa=$eury->result();
			return $semua_sofa;
		}
		public function masuk_sepatu () {
			$this->db->select("*");
			$this->db->from("post");
			$this->db->order_by("id_post","DESK");
			$this->db->limit(6);
			$ruyh=$this->db->get();
			$sepatu_dalam=$ruyh->result();
			return $sepatu_dalam;
		}
		public function post_model($id=false){
			if($id==false){
				$quer=$this->db->get('post');
			}
			else{
				$quer=$this->db->get_where('post',array('id_post'=>$id));
				$post_quer=$quer->result();
				return $post_quer;
			}
		}
		public function update_view($id=false){
			if($id==false){
				echo"<script>alert('data false')</script>";
			}
			else{
				$this->db->select('*');
				$menampil_view=$this->db->get_where('post', array ('id_post'=>$id));
				
				foreach($menampil_view->result() as $rows){
					$news_view=$rows->view;
					$integer_view=$news_view+1;
						$view_update = array (
							'view'=>$integer_view,
						);
					$session_username=$this->session->userdata('nama');
					//echo"<script>alert($session_username)</script>";
					$this->db->select('*');
					$this->db->where(array('username'=>$session_username));
					$view_member=$this->db->get('member');
					foreach ($view_member->result() as $rowp){
							$insert_data = array (
								'id_member'=>$rowp->id_member,
								'id_post'=>$id,
							);
							
							$view_komentar=$this->db->get('view_post',array('id_member'=>$rowp->id_member,'id_post'=>$id))->num_rows();
							if($view_komentar>0){
								//echo"<script>alert('data sudah ada')</script>";	
							}
							else{
								
								$where=array('id_post'=>$id);
								$this->db->update('post',$view_update,$where);
								$this->db->insert('view_post',$insert_data);
							}

					}
				}	
			}
		}
		public function update_like($id=false){
			if($id==false){
				echo"tidak terbaca";
			}
			else{
				$this->db->select('*');
				$menampil_like=$this->db->get_where('post',array('id_post'=>$id));

				foreach($menampil_like->result() as $rows){
					$news_like=$rows->suka;
					$integer_like=$news_like+1;
					$like_update = array(
							'suka'=>$integer_like,
					);
					$session_username=$this->session->userdata('nama');
					//echo"<script>alert($session_username)</script>";
					$this->db->select('*');
					$this->db->where(array('username'=>$session_username));
					$view_member=$this->db->get('member');
					foreach ($view_member->result() as $rowp){
							$insert_data = array (
								'id_member'=>$rowp->id_member,
								'id_post'=>$id,
							);
							$view_suka=$this->db->get('suka_post',array('id_member'=>$rowp->id_member,'id_post'=>$id))->num_rows();
							if($view_suka>0){
								//echo"<script>alert('data sudah ada')</script>";	
							}
							else{	
								$where=array('id_post'=>$id);
								$this->db->update('post',$like_update,$where);
								$this->db->insert('suka_post',$insert_data);
							}
					}	
				}
			}
		}
		public function lihat_komentar ($id=false){
			if($id==false){
				echo"tidak terbaca";
			}else{
				$this->db->select("*");
				$this->db->from('komentar_post');
				$this->db->where(array('id_post'=>$id));
				$eury=$this->db->get();
				$komentar_amat=$eury->result();
				return $komentar_amat;
			}
		}
		public function lihat_username_komentar ($id){
			$this->db->select("*");
			$this->db->from('komentar_post');
			$this->db->where(array('id_post'=>$id));
			$eury=$this->db->get();
			foreach($eury->result() as $rows){
				$where = array (
					'id_member'=>$rows->id_member,
				);
				$komentar_mereka=$this->db->get('member',$where);
				$komentar_semua=$komentar_mereka->result();
				return $komentar_semua;
			}
		}
		public function masukan_komentar () {
			$name_member=$this->session->userdata('nama');
			$this->db->select("*");
			$this->db->from('member');
			$this->db->where(array('username'=>$name_member));
			$query=$this->db->get();
			foreach($query->result() as $rows ){
				$function_data= array (
					'isi'=>$this->input->post('isi'),
					'id_member'=>$rows->id_member,
					'id_post'=>$this->input->post('id_post'),
				);
				$this->db->insert('komentar_post',$function_data);
				$this->db->select('*');
				$menampil_komentar=$this->db->get_where('post',array('id_post'=>$this->input->post('id_post')));

				foreach($menampil_komentar->result() as $rows){
					$news_komentar=$rows->komentar;
					$integer_komentar=$news_komentar+1;
					$komentar_update = array(
							'komentar'=>$integer_komentar,
					);
					$where=array('id_post'=>$this->input->post('id_post'));
					$this->db->update('post',$komentar_update,$where);
				}
			}
		}

		

		public function belajar_modul($id){
			$this->db->select('*');
			$this->db->from("belajar");
			$this->db->where(array ('id_belajar'=>$id));
			$belajarmodul=$this->db->get();
			$belajarsatu=$belajarmodul->result();
			return $belajarsatu;
		}

		public function update_poin ($id) {
			$session_username=$this->session->userdata('nama');
			$qucy=$this->db->get_where('member',array('username'=>$session_username));
			foreach($qucy->result() as $rowv){
				$cuey=$this->db->get_where('belajar',array('id_belajar'=>$id));
				foreach($cuey->result() as $rowb){
					$update_xp=$rowv->xp+$rowb->xp;
					$update_poin=$rowv->xp+$rowb->poin;

					$update_session= array (
						'poin'=>$update_poin,
						'xp'=>$update_xp,
					);
					$where= array ('id_member'=>$rowv->id_member);
					$this->db->update('member',$update_session, $where );
					$belajar_masuk = array (
						'id_member'=>$rowv->id_member,
						'id_belajar'=>$id,
					);
					$this->db->insert('mulai_belajar', $belajar_masuk);
			
				}
			}
		}
		// public function insert_belajar($id) {
			
		// 	$session_user=$this->session->userdata('nama');
		// 	$this->db->select("*");
		// 	$this->db->from("member");
		// 	//$this->db->limit(1);
		// 	$this->db->where(array('username'=>$session_user));
		// 	$y=$this->db->get();
		// 		foreach($y->result() as $rowv){
					

		// 	}
		// }
		public function lihat_seminar () {
			$this->db->select("*");
			$this->db->from("seminar");
			$this->db->order_by("id_seminar","DESK");
			$this->db->limit(4);
			$cuey=$this->db->get();
			$seminar_awal=$cuey->result();
			return $seminar_awal;
		}
		public function seminar_modul($id){
			$this->db->select('*');
			$this->db->from("seminar");
			$this->db->where(array ('id_seminar'=>$id));
			$seminarmodul=$this->db->get();
			$seminarsatu=$seminarmodul->result();
			return $seminarsatu;
		}
		


		public function lihat_lomba () {
			$this->db->select("*");
			$this->db->from("lomba");
			$this->db->order_by("id_lomba","DESK");
			$this->db->limit(4);
			$cuey=$this->db->get();
			$lomba_awal=$cuey->result();
			return $lomba_awal;
		}
		public function lomba_modul($id){
			$this->db->select('*');
			$this->db->from("lomba");
			$this->db->where(array ('id_lomba'=>$id));
			$lombamodul=$this->db->get();
			$lombasatu=$lombamodul->result();
			return $lombasatu;
		}


		public function post_login () {
            $this->db->select("*");
            $this->db->from("post");
            $this->db->order_by("id_post","Desk");
            $this->db->limit(9);
            $eryt=$this->db->get();
            $post_login=$eryt->result();
			return $post_login;
		}
		public function post_search () {
			$this->db->select("*");
			$this->db->from("post");
			$fo=$this->db->get();
			$search_post=$fo->result();
			return $search_post;
		}  


		public function tampil_registrasi ($id) {
			$this->db->select("*");
			$this->db->from("seminar");
			$this->db->where(array('id_seminar'=>$id));
			$cupy=$this->db->get();
			$registrasi_tampil=$cupy->result();
			return $registrasi_tampil;
		}
		public function seminar_poin () {
			$id=$this->input->post('id_seminar');
			$session_username=$this->session->userdata('nama');
			$qucy=$this->db->get_where('member',array('username'=>$session_username));
			foreach($qucy->result() as $rowv){
				$cuey=$this->db->get_where('seminar',array('id_seminar'=>$id));
				foreach($cuey->result() as $rowb){
					$update_xp=$rowv->xp+$rowb->xp;
					$update_poin=$rowv->poin+$rowb->poin;

					$update_session= array (
						'poin'=>$update_poin,
						'xp'=>$update_xp,
					);
					$pembicara_xp=$this->db->get_where('mulai_seminar',array('id_member'=>$rowv->id_member))->num_rows();
						if($pembicara_xp>0){
									echo"<script>alert('sudah terdaftar')</script>";
						}
						else{
							$where= array ('id_member'=>$rowv->id_member);
							$home_poin=$this->db->update('member',$update_session, $where );
							if($home_poin){
								echo"<script>alert('berhasil');</script>";
							}
							




		
							$seminar_masuk = array (
								'id_member'=>$rowv->id_member,
								'id_seminar'=>$id,
								'nama_lengkap'=>$this->input->post('nama_lengkap'),
								'kota_asal'=>$this->input->post('kota_asal'),
								'no_telp'=>$this->input->post('no_telp'),
								'manfaat'=>$this->input->post('manfaat'),
								'tujuan'=>$this->input->post('tujuan'),
							);
							$this->db->insert('mulai_seminar', $seminar_masuk);
			
						}
				}
			}
		}

		public function lomba_template ($id) {
			$this->db->select("*");
			$this->db->from("lomba");
			$this->db->where(array('id_lomba'=>$id));
			$cupy=$this->db->get();
			$registrasi_tampil=$cupy->result();
			return $registrasi_tampil;
		}

		public function lomba_xp () {
			$id=$this->input->post('id_lomba');
			$session_username=$this->session->userdata('nama');
			$qucy=$this->db->get_where('member',array('username'=>$session_username));
			foreach($qucy->result() as $rowv){
				$cuey=$this->db->get_where('lomba',array('id_lomba'=>$id));
				foreach($cuey->result() as $rowb){
					$update_xp=$rowv->xp+$rowb->xp;
					$update_poin=$rowv->poin+$rowb->poin;

					$update_session= array (
						'poin'=>$update_poin,
						'xp'=>$update_xp,
					);
					$pembicara_xp=$this->db->get_where('mulai_lomba',array('id_member'=>$rowv->id_member))->num_rows();
					if($pembicara_xp>0){
								echo"<script>alert('sudah terdaftar')</script>";
					}
					else{

							$where= array ('id_member'=>$rowv->id_member);
							$home_poin=$this->db->update('member',$update_session, $where );
							if($home_poin){
								echo"<script>alert('berhasil');</script>";
							}
							else{
								echo "<script>alert('gagal')</script>";
							}
							
							$seminar_masuk = array (
								'id_member'=>$rowv->id_member,
								'id_lomba'=>$id,
								'nama_lengkap'=>$this->input->post('nama_lengkap'),
								'kota_asal'=>$this->input->post('kota_asal'),
								'no_telp'=>$this->input->post('no_telp'),
								'manfaat'=>$this->input->post('manfaat'),
								'tujuan'=>$this->input->post('tujuan'),
							);
							$this->db->insert('mulai_lomba', $seminar_masuk);

						}
			
				}
			}
		}

		public function lihat_hadiah() {
			$this->db->select("*");
			$this->db->from("hadiah");
			$this->db->order_by("poin_dibutuhkan","Desk");
			$this->db->limit(4);
			$query= $this->db->get();
			$hadiah_lihat=$query->result();
			return $hadiah_lihat;
		}

		public function hadiahsuka() {
			$this->db->select("*");
			$this->db->from("member");
			$this->db->where(array('username'=>$this->session->userdata('nama')));
			$query= $this->db->get();
			$hadiah_session=$query->result();
			return $hadiah_session;
		}

		public function hadiah_template ($id) {
			$this->db->select("*");
			$this->db->from("hadiah");
			$this->db->where(array('id_hadiah'=>$id));
			$cupy=$this->db->get();
			$registrasi_tampil=$cupy->result();
			return $registrasi_tampil;
		}

		public function hadiah_xp () {
			$id=$this->input->post('id_hadiah');
			$session_username=$this->session->userdata('nama');
			$qucy=$this->db->get_where('member',array('username'=>$session_username));
			foreach($qucy->result() as $rowv){
				$cuey=$this->db->get_where('hadiah',array('id_hadiah'=>$id));
				foreach($cuey->result() as $rowb){
					//$update_xp=$rowv->xp+$rowb->xp;
					$update_poin=$rowv->poin-$rowb->poin_dibutuhkan;

					$update_session= array (
						'poin'=>$update_poin,
						//'xp'=>$update_xp,
					);
		
					$where= array ('id_member'=>$rowv->id_member);
					$home_poin=$this->db->update('member',$update_session, $where );
					if($home_poin){
						echo"<script>alert('berhasil');</script>";
					}
					else{
						echo "<script>alert('gagal')</script>";
					}
					
					$seminar_masuk = array (
						'id_member'=>$rowv->id_member,
						'id_hadiah'=>$id,
						'nama_lengkap'=>$this->input->post('nama_lengkap'),
						'kota_asal'=>$this->input->post('kota_asal'),
						'no_telp'=>$this->input->post('no_telp'),
					);
					$this->db->insert('pilih_hadiah', $seminar_masuk);

					
			
				}
			}
		}

		public function member_hadiah ($id) {
			$member_banyak=$this->session->userdata('nama');
			$this->db->select("*");
			$this->db->from("member");
			$this->db->where(array('username'=>$member_banyak));
			$cupy=$this->db->get();
			$regitrasi_member=$cupy->result();
			return $regitrasi_member;
		}

		public function profil_member () {
			$username_profil=$this->session->userdata('nama');
			$this->db->select("*");
			$this->db->from("member");
			$this->db->where(array('username'=>$username_profil));
			$profil_pintu=$this->db->get();
			$halamanmasuk=$profil_pintu->result();
			return $halamanmasuk;
		}

		public function profil_learning () {
			$nama_belajar=$this->session->userdata('nama');
			$this->db->select("*");
			$this->db->from("member");
			$this->db->where(array('username'=>$nama_belajar));
			$belajar_status=$this->db->get();
			
			//$jajan_belajar=$belajar_status->result();
			foreach($belajar_status->result() as $rowt){
				$jajan_belajar=$this->db->get_where('mulai_belajar' , array('id_member'=>$rowt->id_member));
				
				foreach($jajan_belajar->result() as $rowg ){
					$belanja_belajar=$this->db->get_where('belajar' , array('id_belajar'=>$rowg->id_belajar));
					$belanja_poin=$belanja_belajar->result();
					return $belanja_poin;
					
				}
			}
		}

		public function profil_seminar () {
			$nama_belajar=$this->session->userdata('nama');
			$this->db->select("*");
			$this->db->from("member");
			$this->db->where(array('username'=>$nama_belajar));
			$belajar_status=$this->db->get();
			
			//$jajan_belajar=$belajar_status->result();
			foreach($belajar_status->result() as $rowt){
				$jajan_belajar=$this->db->get_where('mulai_seminar' , array('id_member'=>$rowt->id_member));
				
				foreach($jajan_belajar->result() as $rowg ){
					$belanja_belajar=$this->db->get_where('seminar' , array('id_seminar'=>$rowg->id_seminar));
					$seminar_poin=$belanja_belajar->result();
					return $seminar_poin;
					
				}
			}
		}

		public function profil_lomba () {
			$nama_belajar=$this->session->userdata('nama');
			$this->db->select("*");
			$this->db->from("member");
			$this->db->where(array('username'=>$nama_belajar));
			$belajar_status=$this->db->get();
			
			//$jajan_belajar=$belajar_status->result();
			foreach($belajar_status->result() as $rowt){
				$jajan_belajar=$this->db->get_where('mulai_lomba' , array('id_member'=>$rowt->id_member));
				
				foreach($jajan_belajar->result() as $rowg ){
					$belanja_belajar=$this->db->get_where('lomba' , array('id_lomba'=>$rowg->id_lomba));
					$lomba_poin=$belanja_belajar->result();
					return $lomba_poin;
					
				}
			}
		}

		public function profil_hadiah ($id) {
				$hadiah_status=$this->db->get_where('member',array('id_member'=>$id));
				$profil_status=$hadiah_status->result();
				return $profil_status;
		}

		public function rumah_profil ($data) {
			$array_profil = array (
				'username'=>$this->input->post('username'),
				'gambar'=>$data['upload'],
				'email'=>$this->input->post('email'),
				'password'=>$this->input->post('password'),
			);
			$where = array('id_member'=>$this->input->post('id_member'));
			$this->db->update('member',$array_profil,$where);
		}

		public function template_username () {
			$session_username=$this->session->userdata('nama');
			$this->db->where(array('username'=>$session_username));
			$curp=$this->db->get('member');
			$template_member=$curp->result();

			return $template_member;
		}

		// public function fetch_data($query){
		// 	$this->db->from("belajar");
			
		// 	$this->db->order_by('CustomerID','DESK');
		// 	return    $this->db->get();
		// }

		public function lihat_belajar ($query) {
			$this->db->select("*");
			$this->db->from("belajar");
		
			if($query !=''){
				$this->db->like('judulbelajar', $query);
				$this->db->or_like('isibelajar', $query);
				$this->db->or_like('tingkat_kelas', $query);
				$this->db->or_like('keikutsertaan',$query);
				$this->db->or_like('harga', $query);
				$this->db->or_like('untuk_jurusan' , $query);
			}	
			$this->db->limit(4);
			$this->db->order_by("id_belajar","DESK");
			$cuey=$this->db->get();
			$belajar_awal=$cuey->result();
			return $belajar_awal;
		}


	}