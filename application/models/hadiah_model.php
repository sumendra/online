<?php  
        class hadiah_model extends CI_Model {
            public function __construct()  {
                parent:: __construct () ;
                $this->load->database();
            }
            public function lihat_hadiah() {
                $this->db->select("*");
                $this->db->from("hadiah");
                $this->db->order_by("poin_dibutuhkan","Desk");
                $this->db->limit(4);
                $query= $this->db->get();
                $hadiah_lihat=$query->result();
                return $hadiah_lihat;
            }
        }
?>