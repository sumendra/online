<?php  
    class post_model extends CI_Model {
        public function __construct() {
            parent:: __construct();
        }

        public function post_model($id=false){
			if($id==false){
				$quer=$this->db->get('post');
			}
			else{
				$quer=$this->db->get_where('post',array('id_post'=>$id));
				$post_quer=$quer->result();
				return $post_quer;
			}
		}
		public function update_view($id=false){
			if($id==false){
				echo "tidak terbaca";
			}
			else{
				$this->db->select('*');
				$menampil_view=$this->db->get_where('post', array ('id_post'=>$id));
				
				foreach($menampil_view->result() as $rows){
					$news_view=$rows->view;
					$integer_view=$news_view+1;
					$view_update = array (
						'view'=>$integer_view,
					);
					$where=array('id_post'=>$id);		
					$this->db->update('post',$view_update,$where);
				}	
				
			}
		}
		public function update_like($id=false){
				if($id==false){
					echo"tidak terbaca";
				}
				else{
					$this->db->select('*');
					$menampil_like=$this->db->get_where('post',array('id_post'=>$id));

					foreach($menampil_like->result() as $rows){
						$news_like=$rows->suka;
						$integer_like=$news_like+1;
						$like_update = array(
								'suka'=>$integer_like,
						);
						$where=array('id_post'=>$id);
						$this->db->update('post',$like_update,$where);
					}
				}
		}
		public function lihat_komentar ($id){
			$this->db->select("*");
			$this->db->from('komentar_post');
			$this->db->where(array('id_post'=>$id));
			$eury=$this->db->get();
			$komentar_amat=$eury->result();
			return $komentar_amat;
		}
		public function lihat_username_komentar ($id){
			$this->db->select("*");
			$this->db->from('komentar_post');
			$this->db->where(array('id_post'=>$id));
			$eury=$this->db->get();
			foreach($eury->result() as $rows){
				$were = array (
					'id_member'=>$rows->id_member,
				);
				$komentar_mereka=$this->db->get('member',$were);
				$komentar_semua=$komentar_mereka->result();
				return $komentar_semua;
			}
        }
        public function post_lihat () {
            $this->db->select("*");
            $this->db->from("post");
            $this->db->order_by("id_post","Desk");
            $this->db->limit(9);
            $eryt=$this->db->get();
            $post_aery=$eryt->result();
            return $post_aery;
		}
		public function post_search () {
			$this->db->select("*");
			$this->db->from("post");
			$fo=$this->db->get();
			$search_post=$fo->result();
			return $search_post;
		}
    }
?>