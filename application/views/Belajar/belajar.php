<?php  
	if($this->session->userdata('status')!="login"){
		redirect(base_url('user/login'));
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/belajar_post.css') ;?>"> 
    <script src="main.js"></script>
    <style>
        .footer-container {
			margin-top:-10px;
			background-image:url("./Asset/gambar/mulaibelajar.jpeg");
			background-size:cover;
		}
    </style>
</head>
<body>
<div class="col-12 footer-container" style="height:500px;">
		<div style="background:rgba(255, 255, 255,0.4);" class="jumbotron  mb-sm-0 dibawahnav">
			<div class="container">
				<div class="row">
                <!-- <img src="<?php  echo base_url("./Asset/gambar/mulaibelajar.jpeg");?>"> -->
					<div class="row col-sm-4">
					  <h1 style="color:#CE93D8" class="display-4">Kuliah,<small style="color:#42A5F5">Online</small></h1>
					
					  <p class="text-dark "style="font-size:22px;">Raih keinginanmu tanpa batas dan genggam hingga menjadi suatu ha yang nyata</p>
					  <p class="text-muted">awali dengan hal kecil  terlebih dahulu hingga kalian tau apa arti kekalah, kesalahan , 
					  kekeliruan, kegagalan hingga kalian dapat maju terus ke depan mencapai kesuksesan  </p>
					</div>
					<div class="row col-2">
						<div class="col-12">
						</div>
					</div>
					<div class="row col-6 mt-4">
						<div class="col-12 header-belajar">
                            <?php  
                                if(!empty($belajarsatu)){
                                    foreach($belajarsatu as $rowg ){
                            ?>
                            <img src="<?php  echo base_url("./Asset/gambar/");?><?php echo $rowg->gambarbelajar;?>">
                            <?php  
                                    }
                                }
                            ?>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <?php  
        if(!empty($belajarsatu)){
            foreach($belajarsatu as $rowg ){
    ?>
    <div style="background:<?php echo $rowg->warna;?>; height:60px;" class="col-12 events">
        <h1 class="text-light"><?php echo $rowg->judulbelajar;?></h1>
    </div>
    <?php  
            }
        }
    ?>
    <div style="background:#eeeeee; height:140px;" class="col-12 events">
        <div class="row " style="height:100%;">
        <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class=" content " style="margin-left:10%">
                <div class="col-12 ">
                <center>
                    <i class="fas fa-clock dispaly-4 mr-sm-2 " style="font-size:50;color:<?php echo $rowg->warna;?>;"></i>
                </center>
                </div>
                <div class="col-12 " >
                <h5 style="color:<?php echo $rowg->warna;?>;" class="text-center pt-2">waktu</h5>
               
                <p  style="margin-top:-4px;color:<?php echo $rowg->warna;?>;" class="text-center"><?php echo $rowg->waktu;?>bulan</p>  
                </div>
            </div>
        <?php  
                }
            }
        ?>
         <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class="baby" style=" background-color:<?php echo $rowg->warna;?>;" >

            </div>
        <?php  
            }
        }
        ?>
        <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class="content ">
                <div class="col-12 ">
                <center>
                    <i class="fas fa-home dispaly-4 mr-sm-2 "  style="font-size:50;color:<?php echo $rowg->warna;?>;"></i>
                </center>
                </div>
                <div class="col-12 " >
                <h5 style="color:<?php echo $rowg->warna;?>;" class="text-center pt-2" >Sponsor</h5>
               
                <p  style="margin-top:-4px;color:<?php echo $rowg->warna;?>;" class="text-center" ><?php echo $rowg->sponsor;?> </p>  
                </div>
            </div>
        <?php  
                }
            }
        ?>
         <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class="baby" style=" background-color:<?php echo $rowg->warna;?>;">

            </div>
        <?php  
            }
        }
        ?>
        <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class="content ">
                <div class="col-12 ">
                <center>
                    <i class="fas fa-gift dispaly-4 mr-sm-2 " style="font-size:50;color:<?php echo $rowg->warna;?>;" ></i>
                </center>
                </div>
                <div class="col-12 " >
                <h5 style="color:<?php echo $rowg->warna;?>;" class="text-center pt-2" >Hadiah</h5>
               
                <p  style="margin-top:-4px;color:<?php echo $rowg->warna;?>;" class="text-center" ><?php echo $rowg->xp;?>xp dan <?php echo $rowg->poin;?> poin </p>  
            </div>
            </div>
        <?php  
                }
            }
        ?>
         <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class="baby" style=" background-color:<?php echo $rowg->warna;?>;" >

            </div>
        <?php  
            }
        }
        ?>
        <?php  
            if(!empty($belajarsatu)){
                foreach($belajarsatu as $rowg ){
        ?>
            <div class="row content " style="width:24%">
                <div class="col-7 ">
                    <div class="col-12">
                    <center>
                        <i class="fas fa-book dispaly-4 mr-sm-2 pl-4" style="font-size:50;color:<?php echo $rowg->warna;?>;"></i>
                    </center>
                    </div>
                    <div class="col-12 ">
                        <h5 style="color:<?php echo $rowg->warna;?>;" class="mt-0 text-center pl-4 pt-2">Tingkat Kelas</h5>
                       
                        <p style="margin-top:-4px;color:<?php echo $rowg->warna;?>;"class="text-center pl-4" ><?php echo $rowg->tingkat_kelas;?></p> 
                    </div>
                </div>
        <?php  
                }
            }
        ?>
                <div class="col-2">
                    <div style="display:none; margin-left:-200px;position:fixed;bottom:206;z-index:4;transition:2s;" id="demo" ><div class="card"   style="width:400px;"   >
                        <div   class="card-body" >
                        <form>
                            <div class="form-group">
                            <textarea class="form-control" id="editor1" rows="3"></textarea>
                            </div>
                        </form>
                        <a href="data.php" style="background:#37474F;" class="btn text-light">Krirm</a>
                        <a onclick="default_search"  class="btn btn-warning text-light">Cancel</a>
                        </div>
                    </div>
                </div>
                    <a style="position:fixed; bottom:146;z-index:4;" ><i onclick="search()"  style="font-size: 60px; color:9fecf9;"  class="fas fa-address-card"></i></a>
                        <script>
                        function search (){
                            var form=document.getElementById("demo");
                            form.style.display="block";
                            form.setAttribute("onclick","defauld_search()");
                        }
                        function defauld_search () {
                            var form=document.getElementById("demo");
                            form.style.display="none";
                            form.setAttribute("onclick","search()");
                        }
                        </script>
                </div>
                <div class="media col-2 ">
                    <div  id="deno" style="position:fixed;top:340px; background:none; transition:2s; z-index:4; ">
                        <table style="width:200px; height:200px; margin-left:-70px; z-index:4; ">
                            <tr>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:khaki; "></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:white;"> </td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:gray;"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#4da6ff"> </td>
                            </tr>
                            <tr>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#ef5350"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#EC407A"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#AB47BC"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#7E57C2"></td>
                            </tr>
                            <tr>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#5C6BC0"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#42A5F5"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#29B6F6"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#26C6DA"></td>
                            </tr>
                            <tr>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#26A69A"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#66BB6A"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#9CCC65"></td>
                                <td onclick="bgChange(this.style.backgroundColor)" style="background-color:#D4E157"></td>
                            </tr>
                        </table>
                    
                    </div>
                    <a style="position:fixed; bottom:146px;" onclick="attribut()" ><i style="font-size:60px; color:#CE93D8" class="fab fa-audible"></i></a>
                    <script>
                        function attribut (){
                            var form = document.getElementById("deno");
                            form.style.display="block";
                            form.setAttribute("onclick","defaul()");
                        }
                        function defaul (){
                            var form = document.getElementById("deno");
                            form.style.display="none";
                            form.setAttribute("onclick","attribut()");
                        }
                        function bgChange (bg) {
                            document.body.style.background = bg ;
                        }
                    </script>
                </div>
            </div>
        </div>
    </div>
    <section>
			<div class="isilearning">
				<div class="ju">
                <?php  
                    if(!empty($belajarsatu)){
                        foreach($belajarsatu as $rowg ){
                ?>
                    <div class="ta" style="border: 10px solid <?php echo $rowg->warna;?>;">
               
						<h4 style="color:<?php echo $rowg->warna;?>">Selamat Datang </h4> 
						<h4 style="color:<?php echo $rowg->warna;?>">di Tutorial Html dan Css </h4>
					</div>
					<div class="j">
						<a href="../Learning/isilearningpengertianhtml.php" style="color:<?php echo $rowg->warna;?>"><i class="fas fa-play-circle" style="color:<?php echo $rowg->warna;?>"></i> Pengenalan awal html dan css dasar </a>
						<a href="../Learning/isilearningpengertianattribut.php" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i>mengenal html element dan attribut</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal css warna dan background</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-play-circle"style="color:<?php echo $rowg->warna;?>" ></i> mengenal html paragraf</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal css width dan height  </a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal html formatif dan kutipan</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal css text dan font</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-play-circle" style="color:<?php echo $rowg->warna;?>" ></i> mengenal html komentar dan warna</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal Css border, margin , dan.... </a>
				
						<a href="../Learning/isilearningquisperbedaanpadding.php" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-star" style="color:<?php echo $rowg->warna;?>"></i> apa perbedaan antara padding dan.... </a>
							
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal html image dan html table</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-play-circle" style="color:<?php echo $rowg->warna;?>" ></i> mengenal html form </a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal css float dan overflow</a>
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal html layout </a>
							
						<a href="" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-play-circle" style="color:<?php echo $rowg->warna;?>" ></i> mengenal css opacity dan navigation bar</a>
						<a href="../Learning/isilearningpengertiancanvas.php" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-book" style="color:<?php echo $rowg->warna;?>" ></i> mengenal html canvas dan map </a>
						<a href="../Learning/isilearningpengertiandropdown.php" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-play-circle" style="color:<?php echo $rowg->warna;?>" ></i> mengenal css dropdown dan media query</a>
						<a href="../tubes.php" style="color:<?php echo $rowg->warna;?>" ><i class="fas fa-trophy" style="color:<?php echo $rowg->warna;?>" ></i> Buatlah sebuah website sesuai tema .... </a>
					</div>
                </div>
                <?php  
                    }
                }
                ?>
				<div class="h">
						<h1>Selamat Datang Di Modul Web Development Pemula </h1>
								<img id="myImg" src="<?php  echo base_url("./Asset/gambar/fa.jpg");?>" alt="Contoh Wesite" width="300" height="200">
								<div id="myModal" class="modal">
								  <span class="close">×</span>
								  <img class="modal-content" id="img01">
								  <div id="caption"></div>
								</div>
								<script>
								var modal = document.getElementById('myModal');
								var img = document.getElementById('myImg');
								var modalImg = document.getElementById("img01");
								var captionText = document.getElementById("caption");
								img.onclick = function(){
									modal.style.display = "block";
									modalImg.src = this.src;
									captionText.innerHTML = this.alt;
								}
								var span = document.getElementsByClassName("close")[0];
								span.onclick = function() { 
									modal.style.display = "none";
								}
								</script>
						<div class="tess">
						<p>&nbsp &nbsp &nbsp &nbsp    Dalam kelas ini kami akan mengajarkan kalian html dan css dari dasar sampai expert. kami menyediakan 16 modul yang sangat akan membantu kalian dalam pembelajaran. kami juga menyediakan sekitar 2 quis dan 1 tubes sebagai ujian buat kalian. dilarang mengcopy modul, quis dan tubes yang kami siapkan karena telah di patenkan dalam uu dasar no 22 tahun 2012 tentang hak cipta. semoga dengan adanya kelas ini dapat membantu kalian dalam pemrograman design web. setelah menyelesaikan modul ini di harapkan kalian dapat ,mendesign web yang baik.</p>
						</div>
						
				</div>
				
			</div>		
		</section>
</body>
</html>