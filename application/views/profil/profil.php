<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/lomba.css') ;?>">
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/profil.css') ;?>">
    <script src="main.js"></script>
    <style> 
        .footer-container {
			margin-top:-10px;
			background-image:url("../Asset/gambar/profil_member.jpeg");
			background-size:cover;
		}
    </style>
</head>
<body>
<header>
<div class="col-12 footer-container" style="height:500px;margin-top:20px;">
		<div style="background:rgba(255, 255, 255,0.4);" class="jumbotron  mb-sm-0 dibawahnav">
			<div class="container">
				<div class="row">
					<div class="row col-sm-4">
					  <h1 style="color:#CE93D8" class="display-4">Kuliah,<small style="color:#42A5F5">Online</small></h1>
					
					  <p class="text-dark "style="font-size:22px;">Raih keinginanmu tanpa batas dan genggam hingga menjadi suatu ha yang nyata</p>
					  <p class="text-muted">awali dengan hal kecil  terlebih dahulu hingga kalian tau apa arti kekalah, kesalahan , 
					  kekeliruan, kegagalan hingga kalian dapat maju terus ke depan mencapai kesuksesan  </p>
					</div>
					<div class="row col-2">
						<div class="col-12">
						</div>
					</div>
					<div class="row col-6 mt-4">
						<div class="col-12">
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<button class="tablink" onclick="openPage('Home', this, '#78909C')">Tentang Saya</button>
			<button class="tablink" onclick="openPage('News', this, '#78909C')" id="defaultOpen">Learning</button>
			<button class="tablink" onclick="openPage('Contact', this, '#78909C')">Events</button>
			<button class="tablink" onclick="openPage('About', this, '#78909C')">Challenge</button>
           
            <?php  
                if(!empty($halamanmasuk)){
                    foreach($halamanmasuk as $rowh){
            ?>
            <div id="Home" class="tabcontent">
           
                <h2>Biodata</h2>
                <p>Nama &emsp; &nbsp; &nbsp; &nbsp; &nbsp;   &nbsp; <?php  echo $rowh->username;?> </p>
                <p>Email &emsp; &nbsp; &nbsp; &nbsp; &nbsp;   &nbsp; <?php  echo $rowh->email;?> </p>
                <p>Status &nbsp; &nbsp; &nbsp;   &nbsp; &nbsp; &nbsp;&nbsp;    &nbsp; <?php  echo $rowh->status;?> </p>
                <p>Xp &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php  echo $rowh->xp;?></p>
                <p>Poin &emsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp;   &nbsp; <?php  echo $rowh->poin;?> </p> <br/>
                    <a style="color:white;" >
                        <a class="btn " href="<?php  echo base_url('halamanutama/profil/edit');?>/<?php  echo $rowh->id_member;?>" style="border: none;border-radius: 4px;padding: 8px 16px; cursor: pointer; background:#37474F; color:white;">
                            Edit Profile
                        </a>
                    </a>
            </div>
            <?php  
                    }
                }
                else{
                    echo"data kosong";
                }
            ?>	

            <?php  
                if(!empty($belanja_poin)){
                    foreach($belanja_poin as $rowl){
            ?>
            <div id="News" class="tabcontent">
                <center><h2>Belajar</h2></center>
                <div class="ex3">
                    <div class="container" style="margin-top:60px;">
                        <div class="row">
                                <div class=" col-12 atas-belajar">
                                    <div class="row">
                                        <div class="penjelasan">
                                                <div class=" col-12 header-belajar" style="background:<?php  echo $rowl->warna;?>">
                                                        <p class="text-center text-light"><?php  echo $rowl->judulbelajar;?></p>
                                                </div>
                                                <div class=" col-12 tubuh-belajar">
                                                    <div class="row">
                                                        <div class="belajar-gambar">
                                                                <img src="../Asset/gambar/<?php  echo $rowl->gambarbelajar;?>">
                                                        </div>
                                                        <div class="belajar-teks">
                                                                <p><?php  echo $rowl->isibelajar;?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="pebatas" style="background:<?php  echo $rowl->warna;?>">

                                        </div>
                                        <div class="fitur">
                                                <div class="col-12 atas-fitur" style=" background:<?php  echo $rowl->warna;?>">
                                                            <p class="text-center text-light">Fitur Fitur </p>
                                                </div>
                                                <div class="col-12 bawah-fitur">
                                                            <h1 style="padding-top:6%;"><i class="fas fa-user" style="padding-right:6px;"></i><?php  echo $rowl->siswa;?>Siswa</h1>
                                                            <h1><i class="fas fa-gift"  style="padding-right:6px;"></i><?php  echo $rowl->poin;?> poin <?php  echo $rowl->xp;?> xp</h1>
                                                            <h1><i class="fab fa-wpbeginner"  style="padding-right:6px;"></i><?php  echo $rowl->tingkat_kelas;?></h1>
                                                            <h1><i class="fas fa-unlock-alt"  style="padding-right:6px;"></i><?php  echo $rowl->keikutsertaan;?></h1>
                                                            <h1><i class="fab fa-bitcoin"  style="padding-right:6px;"></i><?php  echo $rowl->harga;?></h1>
                                                </div>
                                        </div>

                                        <div class=" col-12 bawah-belajar">
                                            <center>
                                                <a  class="btn btn text-light" style="background:<?php  echo $rowl->warna;?>;">Mulai Belajar</a>
                                            </center>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php  
                    }
                }
                else{
                    echo"data kosong";
                }
            ?>

            <?php  
                    if(!empty($seminar_poin)){
                        foreach($seminar_poin as $rowf){
            ?>
            <div id="Contact" class="tabcontent">
                <center><h2>Seminar</h2></center>
                <div class="ex3">
                    <div class="container" style="margin-top:60px;">
                        <div class="row">
                                <div class=" col-12 atas-belajar">
                                    <div class="row">
                                        <div class="penjelasan">
                                                <div class=" col-12 header-belajar" style="background:<?php  echo $rowf->warna;?>">
                                                        <p class="text-center text-light"><?php  echo $rowf->judulseminar;?></p>
                                                </div>
                                                <div class=" col-12 tubuh-belajar">
                                                    <div class="row">
                                                        <div class="belajar-gambar">
                                                                        <img src="../Asset/gambar/<?php  echo $rowf->gambarseminar;?>">
                                                        </div>
                                                        <div class="belajar-teks">
                                                                <p><?php  echo $rowf->isiseminar;?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="pebatas" style="background:<?php  echo $rowf->warna;?>">

                                        </div>
                                        <div class="fitur">
                                                <div class="col-12 atas-fitur" style=" background:<?php  echo $rowf->warna; ?>">
                                                            <p class="text-center text-light">Fitur Fitur </p>
                                                </div>
                                                <div class="col-12 bawah-fitur">
                                                            <h1 style="padding-top:6%;"><i class="fas fa-user" style="padding-right:6px;"></i><?php  echo $rowf->siswa;?>Siswa</h1>
                                                            <h1><i class="fas fa-gift"  style="padding-right:6px;"></i><?php  echo $rowf->poin;?> poin <?php  echo $rowf->xp;?> xp</h1>
                                                            <h1><i class="fab fa-wpbeginner"  style="padding-right:6px;"></i><?php  echo $rowf->tingkat_kelas;?></h1>
                                                            <h1><i class="fas fa-unlock-alt"  style="padding-right:6px;"></i><?php  echo $rowf->keikutsertaan;?></h1>
                                                            <h1><i class="fab fa-bitcoin"  style="padding-right:6px;"></i><?php  echo $rowf->harga;?></h1>
                                                </div>
                                        </div>

                                        <div class=" col-12 bawah-belajar">
                                            <center>
                                                <a  class="btn btn text-light" style="background:<?php  echo $rowf->warna;?>;">Mulai Seminar</a>
                                            </center>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php  
                        }
                    }
            ?>

            <?php  
                    if(!empty($lomba_poin)){
                        foreach($lomba_poin as $rowg){
            ?>
            <div id="About" class="tabcontent">
                <center><h2>Lomba</h2></center>
            <div class="ex3">
                <div class="container" style="margin-top:60px;">
                    <div class="row">
                            <div class=" col-12 atas-belajar">
                                <div class="row">
                                    <div class="penjelasan">
                                            <div class=" col-12 header-belajar" style="background:<?php  echo $rowg->warna;?>;">
                                                    <p class="text-center text-light"><?php  echo $rowg->judullomba;?></p>
                                            </div>
                                            <div class=" col-12 tubuh-belajar">
                                                <div class="row">
                                                    <div class="belajar-gambar">
                                                                    <img src="../Asset/gambar/<?php  echo $rowg->gambarlomba;?>">
                                                    </div>
                                                    <div class="belajar-teks">
                                                            <p><?php  echo $rowg->isilomba;?></p>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="pebatas" style="background:<?php  echo $rowg->warna;?>">

                                    </div>
                                    <div class="fitur">
                                            <div class="col-12 atas-fitur" style=" background:<?php  echo $rowg->warna;?>">
                                                        <p class="text-center text-light">Fitur Fitur </p>
                                            </div>
                                            <div class="col-12 bawah-fitur">
                                                        <h1 style="padding-top:6%;"><i class="fas fa-user" style="padding-right:6px;"></i><?php  echo $rowg->siswa;?>Siswa</h1>
                                                        <h1><i class="fas fa-gift"  style="padding-right:6px;"></i><?php  echo $rowg->poin;?> poin <?php  echo $rowg->xp;?> xp</h1>
                                                        <h1><i class="fab fa-wpbeginner"  style="padding-right:6px;"></i><?php  echo $rowg->tingkat_kelas;?></h1>
                                                        <h1><i class="fas fa-unlock-alt"  style="padding-right:6px;"></i><?php  echo $rowg->keikutsertaan;?></h1>
                                                        <h1><i class="fab fa-bitcoin"  style="padding-right:6px;"></i><?php  echo $rowg->harga;?></h1>
                                            </div>
                                    </div>

                                    <div class=" col-12 bawah-belajar">
                                        <center>
                                            <a  class="btn btn text-light" style="background:<?php  echo $rowg->warna;?>;">Mulai lomba</a>
                                        </center>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php  
                        }
                    }
        ?>
    </div>
    <script>
        function openPage(pageName,elmnt,color) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
                for (i = 0; i < tabcontent.length; i++) {
                    tabcontent[i].style.display = "none";
                }
                tablinks = document.getElementsByClassName("tablink");
                for (i = 0; i < tablinks.length; i++) {
                    tablinks[i].style.backgroundColor = "";
                }
                document.getElementById(pageName).style.display = "block";
                elmnt.style.backgroundColor = color;
            }
            document.getElementById("defaultOpen").click();
        </script>
</body>
</html>