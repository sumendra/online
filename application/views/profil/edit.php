<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" type="text/css" href="<?php  echo base_url('./Asset/css/profiledit.css') ?>">
    <script src="main.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>
<body>

<?php  
        if(!empty($profil_status)){
            foreach($profil_status as $rowg){
                    $profil_poin=$rowg->id_member;
                    $hidden['id_member']=$profil_poin;
?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <!-- UPLOAD IMAGE -->
                <?php  echo validation_errors(); ?>
                <form class="form-horizontal"   <?php  echo form_open_multipart('halamanutama/tutorial_profil','',$hidden)?> 

                <fieldset>

                <!-- Form Name -->
                <legend>User profile</legend>

                <!-- Text input-->

                <div class="form-group">
                <label class="col-md-4 control-label" for="Name (Full name)">Nama (Username)</label>  
                <div class="col-md-4">
                    <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-user">
                        </i>
                    </div>
                    <input id="Name" name="username" value="<?php  echo $rowg->username;?>"type="text"  class="form-control input-md">
                    </div>
                </div>  
                </div>

                <!-- File Button --> 
                <div class="form-group">
                <label class="col-md-4 control-label" for="Upload photo">Upload Photo</label>
                <div class="col-md-4">
                    <input id="Upload photo" name="upload" class="input-file" type="file">
                </div>
                </div>

                <!-- Text input-->


                <div class="form-group">
                <label class="col-md-4 control-label" for="Email Address">Email</label>  
                <div class="col-md-4">
                    <div class="input-group">
                    <div class="input-group-addon">
                            <i class="fa fa-envelope-o"></i>
                    </div>
                            <input name="email" value="<?php  echo $rowg->email;?>"type="email" class="form-control input-md" >
                    </div>
                </div>
                </div>


                <div class="form-group">
                <label class="col-md-4 control-label" for="Phone number ">Password </label>  
                <div class="col-md-4">
                    <div class="input-group othertop">
                        <div class="input-group-addon">
                            <i class="fa fa-mobile fa-1x" style="font-size: 20px;"></i>
                        </div>
                            <input id="Phone number " value="<?php  echo $rowg->password;?>"name="password" type="text"  class="form-control input-md">
                    </div>
                </div>
                </div>


                <div class="form-group">
                <label class="col-md-4 control-label" ></label>  
                <div class="col-md-4">
                    <button class="btn btn-success" type="Submit" ><span class="glyphicon glyphicon-thumbs-up"></span> Update
                    </button>
                <a  href="<?php  echo base_url('halamanutama/cancel_profil')?>"class="btn btn-danger" type="cancel"><span class="glyphicon glyphicon-remove-sign"></span> Cancel </a>
                </div>
                </div>
                </fieldset>
                </form>
            </div>
            <div class="col-md-2 hidden-xs">

            </div>
        </div>
    </div>

    <?php  
            }
        }
    ?>
    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>