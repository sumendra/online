<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/hadiah.css') ;?>">
    <script src="main.js"></script>
</head>
<body>
    <div class="col-12">
        <div class="container-fluid" style="margin-top:-10px;">
            <div class="col-12" style="height:40px;">
                    
            </div>
            <div class="row mt-4" >
                <div class="penjelasan" style="height:40px;">
                <h1>Hadiah</h1>
                
                </div>
                <div class="hadiah" style="height:40px;background:white">
                    <p class="pl-2">My Poin : 0</p>
                </div>
            </div>
            <div class="col-12" style="height:30px;">
                    
            </div>
           
            <div class="row" >
                <div class="penjelasan">
                <p>hadiah merupakan fitur dari kuliah online dimana kalian dapat menukarkan point yang kalian dapatkan sebelumnya
                dengan hadiah hadiah yang telah kami sediakan diantaranya baju laptop buku dan lain sebagainya </p>
                </div>
                <div class="hadiah ml-4">
                <?php  
                    if(!empty($hadiah_lihat)){
                        foreach($hadiah_lihat as $rows){
                ?>
                    <div class="row " style="background:#EEEEEE;" >
                        <div class="image ml-2">
                             <img src="<?php  echo base_url('Asset/gambar') ?>/<?php echo $rows->gambarhadiah ; ?>">
                        </div>
                        <div class="content-hadiah  ">
                                <h1><?php  echo $rows->judulhadiah; ?>  </h1>
                                <p> <?php  echo $rows->deskripsihadiah; ?> </p>
                                <p> <?php  echo $rows->poin_dibutuhkan; ?> Poin</p>
                                <button class="btn btn-primary">Ambil Hadiah</button>
                        </div>
                    </div>
                    <div class="col-12" style="height:30px;">
                    
                    </div>
                <?php  
                        }
                    }
                    else{
                        echo"data kosong";
                    }
                ?>
                </div>
            </div>
            <div class="pagination" style="margin-left:32%;">
					<a href="#">&laquo;</a>
					<a href="#">1</a>
					<a class="active" href="#">2</a>
					<a href="#">3</a>
					<a href="#">4</a>
					<a href="#">5</a>
					<a href="#">6</a>
					<a href="#">7</a>
					<a href="#">8</a>
					<a href="#">9</a>
					<a href="#">10</a>
					<a href="#">&raquo;</a>
            </div>
            <div class="col-12" style="height:30px;">
                    
            </div>
        </div>
    </div>
</body>
</html>