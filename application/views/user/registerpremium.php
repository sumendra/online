<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/registerpremium.css') ;?>"> 
    <script src="main.js"></script>
    <style>
        .open{
            background-image:url("../Asset/gambar/registrasipremium.jpg");
			background-repeat:no-repeat;
			background-size:100%;
        }
        .footer-container{
            background-image:url("../Asset/gambar/registrasipremium.jpg");
			background-repeat:no-repeat;
			background-size:100%;
        }
        .content-registrasi h1{
            color:white;
            padding-left:2%
        }
        .syarat-content{
            width:97%;
            margin-left:1.5%;
            height:87.5%;
            background-image:url("../Asset/gambar/home.jpg");
        }
        .component-content{
            width:97%;
            margin-left:1.5%;
            height:97%;
            background-image:url("../Asset/gambar/loginbackground.jpeg");
            margin-top:3%;
        }
        .content-registrasi p{
            font-size:17px;
            color:white;
            padding-left:4%;
        }
        .selisih{
            height:0.4%;
            width:80%;
            margin-left:10%;
            background:white;
            margin-top:2%;
        }
        .selisih-content{
            height:0.4%;
            width:80%;
            margin-left:10%;
            background:#000000;
            margin-top:4%;
        }
        .judul-content h1{
            padding-left:2%
        }
        .syarat-content p{
            font-size:17px;
            padding-left:4%;
        }
        .component-content h1{
            padding-left:2%
        }
        .component-content p{
            font-size:17px;
            padding-left:4%;
        }
        .syarat-conten{
            width:98%;
            margin-left:1.5%;
            height:100%;
            background:rgba(255, 255, 255,0.6);
        }
        .component-conten{
            width:100%;
            height:100%;
            background:rgba(255, 255, 255,0.6);
            margin-top:3%;
        }
    </style>
</head>
    <body>
        <div class=" open">
            <div class="col-12 conver">
                <div class=" col-12 code-start  ">
                    <h1>Mulai Dari Hal Yang Kecil </h1>
                    <p>didukung oleh pembelajaran berbagai modul yang bermanfaat</p>
                </div>
                <div class="col-12 content-conver">
                    <div class="content-registrasi">
                        <h1 class="mt-2">Profesional</h1>
                        <p>kamu akan mendapatkan tutorial video setiapharinya sesuai dengan yang kamu inginkan </p>
                        <p>hanya dengan 200.000/bulan</p>
                        <div class="selisih">

                        </div>
                        <p style="padding-top:10%;"><i class="fas fa-angle-right" style="color:white;padding-right:2%;"></i>setiap hari mendapat 1 modul</p>
                        <p><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap hari dapat bertanya tentang pelajaran</p>
                        <p><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap waktu mensupport kamu dalam tugas dan project</p>
                        <p><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap waktu dapat memberikan materi materi  </p>
                        <div class="selisih">

                        </div>
                        <center>
                            <a href="<?php  echo base_url('user/registrasi_profesional');?>" class="btn btn-primary mt-2 "><i class="fas fa-hourglass-start"style="color:white; padding-right:4%;"></i>Mulai Belajar</a>
                        </center>
                    </div>
                    <div class="content-premium">
                        <div class="judul-content">
                            <h1 style="padding-top:2%;">Basic</h1>
                        </div>
                        <div class="syarat-content">
                            <div class="syarat-conten">
                                <p style="padding-top:8%;">kamu akan mendapatkan tutorial video setiap harinya sesuai dengan yang kamu inginkan </p>
                                <p>hanya dengan 100.000/bulan</p>
                                <div class="selisih-content">

                                </div>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;padding-top:10%;"></i>setiap hari mendapat 1 modul</p>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;"></i>setiap hari dapat bertanya tentang pelajaran</p>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;"></i>setiap waktu mensupport kamu dalam tugas dan project</p>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;"></i>setiap waktu dapat memberikan materi materi  </p>
                                <div class="selisih-content">

                                </div>
                                <center>
                                    <a href="<?php  echo base_url('user/registrasi_basic');?>"  class="btn btn-primary " style="margin-top:64px;"><i class="fas fa-hourglass-start"style="color:white; padding-right:4%;"></i>Mulai Belajar</a>
                                </center>
                             </div>
                        </div>
                    </div>
                    <div class="content-premium">
                        <div class="component-content">
                            <div class="component-conten">
                                <h1  style="padding-top:2%;">Team learn</h1>
                                <p style="padding-top:4%;">kamu akan mendapatkan tutorial video setiapharinya bersama dengan timmu </p>
                                <p>hanya dengan 200.000/bulan</p>
                                <div class="selisih-content">

                                </div>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;padding-top:10%"></i>setiap hari mendapat 1 modul untuk setiap orang</p>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;"></i>setiap hari dapat bertanya tentang pelajaran</p>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;"></i>setiap waktu mensupport kamu dalam tugas dan project</p>
                                <p><i class="fas fa-angle-right" style="padding-right:2%;"></i>setiap waktu dapat memberikan materi materi  </p>
                                <div class="selisih-content">

                                </div>
                                <center>
                                    <a   href="<?php  echo base_url('user/registrasi_teamlearn');?>"class="btn btn-primary"   style="margin-top:60px;"><i class="fas fa-hourglass-start"style="color:white; padding-right:4%;"></i>Mulai Belajar</a>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="content-registrasi">
                            <h1 class="mt-2">Expert</h1>
                            <p>kamu akan mendapatkan tutorial video setiap harinya sesuai dengan yang kamu inginkan </p>
                            <p>hanya dengan 400.000/bulan</p>
                            <div class="selisih">

                            </div>
                            <p style="padding-top:10%;"><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap hari mendapat 1 modul</p>
                            <p><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap hari dapat bertanya tentang pelajaran</p>
                            <p><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap waktu mensupport kamu dalam tugas dan project</p>
                            <p><i class="fas fa-angle-right"style="color:white;padding-right:2%;"></i>setiap waktu dapat memberikan materi materi  </p>
                            <div class="selisih">

                            </div>
                            <center>
                                <a href="<?php  echo base_url('user/registrasi_expert');?>"class="btn btn-primary mt-2"style="color:white;padding-right:2%;"><i class="fas fa-hourglass-start" style="color:white; padding-right:4%;"></i>Mulai Belajar</a>
                            </center>
                    </div>
                </div>
            </div>
        </div> 
        <div class="testimoni">
			<div class="content-testimoni">
				<p> Testimoni </p>
			</div>
			<div class="testimoni-select">
				<div id="testimoni" style="margin-left:8%;" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Eka baskara</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan. 
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya 
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				<div id="testimonia" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Rio Hardianto</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan. 
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya 
						kuliah online the best online for lecturer  </p>
					</div>
					
				</div>
				<div id="testimonib" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Ari susila</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan.
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				
				<div id="testimonid" style="margin-left:8%;margin-top:2%;" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Lingga Wacika</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan.
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				<div id="testimonie"  style="margin-top:2%;"class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Daerah Sutarsono</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan. 
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya 
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				<div id="testimonif"style="margin-top:2%;" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Ramdani Suki</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan. 
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				<div id="testimonig" style="margin-left:8%;margin-top:2%;" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Danis Zaidan</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan. 
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya 
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				<div id="testimonih" style="margin-top:2%;"class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Poncut ridha</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan.
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				<div id="testimonii" style="margin-top:2%;" class="testimoni-kartu">
					<div style="background:#37474F;"class="header-testimoni">
					</div>
					<div style="background:#37474F;"class="profil-testimoni">
					</div>
					<div class="part-testimoni">
						<h1>Taupiq Hidayat</h1>
						<p> &nbsp &nbsp menurut saya belajar di sini sangat complit baik itu dari segi modul, seminar dan perlombaan.
						tetep semangat buat admin sukses selalu dan jangan lupa untuk tetap bangun pendidikan di indonesia . pokoknya 
						kuliah online the best online for lecturer  </p>
					</div>
				</div>
				
			</div>
		</div>
		<script>
			window.onscroll = function() { myFunction()};
			
			function myFunction(){	
				 if(document.body.scrollTop > 800 || document.documentElement.scrollTop > 800){
					document.getElementById("testimonig").className="testUp";
					document.getElementById("testimonih").className="testUp";
					document.getElementById("testimonii").className="testUp";
				}
			    if(document.body.scrollTop >700 || document.documentElement.scrollTop > 700){
					document.getElementById("testimonid").className="testUp";
					document.getElementById("testimonie").className="testUp";
					document.getElementById("testimonif").className="testUp";
				}
				if(document.body.scrollTop > 400 || document.documentElement.scrollTop > 400 ){
					document.getElementById("testimoni").className="testUp";
					document.getElementById("testimonia").className="testUp";
					document.getElementById("testimonib").className="testUp";
				}
				else{
					document.getElementById("testimoni").className = "testimoni-kartu";
					document.getElementById("testimonia").className="testimoni-kartu";
					document.getElementById("testimonib").className="testimoni-kartu";
					document.getElementById("testimonid").className="testimoni-kartu";
					document.getElementById("testimonie").className="testimoni-kartu";
					document.getElementById("testimonif").className="testimoni-kartu";
					document.getElementById("testimonig").className="testimoni-kartu";
					document.getElementById("testimonih").className="testimoni-kartu";
					document.getElementById("testimonii").className="testimoni-kartu";
				}
					
			}
			
		</script>
    </body>
</html>