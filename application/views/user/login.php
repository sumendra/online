<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Page Title</title>
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<script src="http://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/bootstrap/bootstrap.css'); ?>">	
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/index.css') ;?>"> 
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/template.css') ;?>"> 
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/login.css') ;?>"> 
   		<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		body{
			background-image:url("../Asset/gambar/loginbackground.jpeg");
			background-repeat:no-repeat;
			background-size:100%;
		}
	</style>
</head>
<body>
<div class="container form_login">
	<div class="row">
		<div class="col-3">
		</div>
		<div class="col-5 border border-primary mt-3 login">
			<div class="row">
				<div class="col-12 text-light py-sm-3 biodata">
					<h4 class="text-center text-light">Masukkan Biodata Login</h4>
				</div>
			</div>
			<?php  echo validation_errors(); ?>
			<?php  echo form_open (base_url('user/login'));?>			
					<div class="row atas">
						<div class="form-group col-12">
							<label>Username</label>
							<input type="text" name="username" class="form-control col-12" placeholder="enter your Username" >
						</div>
					</div>
					<div class="row">
						<div class="form-group col-12 mt-4">
							<label>Password</label>
							<input type="password" name="password" class="form-control col-12" placeholder="enter your password">
						</div>
					</div>
						<button class="btn btn-primary ml-2 mr-sm-2 mt-4" type="submit">Masuk</button>
						<a href="<?php echo base_url('user/cancellogin');?>" class="btn btn-warning text-light mt-4 ml-2">Cancel</a>
			</form>
		</div>
	</div>
</body>
</html>