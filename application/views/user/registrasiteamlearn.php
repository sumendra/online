<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
    <script src="http://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/bootstrap/bootstrap.css'); ?>">	
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/index.css') ;?>"> 
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/template.css') ;?>"> 
    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/registrasiteamlearn.css') ;?>">
    <script src="main.js"></script>
    <style>
        .header-container{
            background-image:url("../Asset/gambar/registrasiteamlearn.jpeg");
            background-repeat:no-repeat;
            background-size:cover;
            height:1400px;
            margin-top:-0.9%;
            width:100%
        }
        .footer-container {
            background-image:url("../Asset/gambar/registrasiteamlearn.jpeg");
            background-repeat:no-repeat;
            background-size:cover;
        }
  
    </style>
</head>
<body>
    <header>
        <div class="hover">
            <div class="toggle">
                <div class="logi"><a href="<?php  echo base_url('');?>"><img src="<?php  echo base_url('Asset/gambar/logo.png');?>"></a></div>
                <i class="fas fa-bars menu"></i>	
            </div>
            <ul>
                <div class="logo"><a href="<?php echo base_url(''); ?>"><img src="<?php  echo base_url('Asset/gambar/logo.png');?>"></a></div>
                    <li><a style="color:#37474F;" href="<?php echo base_url('');  ?>"><i class="fas fa-graduation-cap"></i>Belajar</a></li>
                    <li><a  style="color:#37474F;" href="<?php  echo base_url('');  ?>"><i class="far fa-calendar-alt"></i></i>Seminar</a></li>
                    <li class="drop">
                    <a href="" class="dropb"><i class="fas fa-trophy"></i>Lomba</a>
                    <div class="drop-content">
                        <a href=""><i class="fas fa-key"></i>Masih Berlangsung</a>
                        <a href="#"><i class="fas fa-lock"></i>Sudah Selesai</a>
                    </div>
                    </li>
                    <li><a style="color:#37474F;" href="reward.php"><i class="fas fa-newspaper"></i>Post</a></li>
                    <li><a style="color:#37474F;" href="reward.php"><i class="fas fa-gift"></i>Hadiah</a></li>
                <form class="form-inline mt-1 ml-4 col-6">
                    <input style="width:400px;" class="form-control mr-sm-2 ml-2 mt-2 mt-1 left" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-primary  mt-2" type="submit">Search</button>
                </form>
            <div class="login" style="margin-top:-56px;">
                    <li><a href="<?php echo base_url('user/login');?>"><i class="fas fa-sign-in-alt" style="color:white;"></i>Login</a></li>
            </div>
            </ul>
        </div>
    </header>
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.menu').click(function(){
                    $('ul').toggleClass('active');
                    })
                })
        </script>
    <div class=" header-container">
        <div class=" col-12 header-content">
            <h1 class=" text-center" style="padding-top:100px;">Class TeamLearn</h1>
            <p class="text-center">Raih kesempatan belajar bersama Develover TeamLearn</p>
            <div class="registrasi-content ">
                <div class="pembuka-profesional">
                        <p>Mulai Class TeamLearn Hari Ini </p>
                </div>
                <div class="tubuh-profesional">
                        <div class="tahap-awal">
                            <h1 class="pl-4 pt-4"><i class="fab fa-bandcamp" style="margin-right:7px;"></i>Pilih Jurusan Yang Kalian Minat</h1>
                            <form method="GET">
                                <div class="fulljava col-12">
                                    <input type="submit" name="front_end"  id="satu" onclick="pilih_satu()" style="margin-left:60px;" value="Full Front End Develovment" class="btn btn col-5  mt-2 mt-2" >
                                    <input type="submit" name="deskop"  id="dua" onclick="pilih_dua()" value="Full deskop Aplication Development" class="btn btn col-5 mt-2 ml-2">
                                </div>
                                <div class="fullphton col-12">
                                    <input type="submit" name="back_End" id="tiga" onclick="pilih_tiga()" value="Full Back End Development" style="margin-left:60px;"class="btn btn col-5 mt-2"> 
                                    <input type="submit" name="aplication_mobile" id="empat" onclick="pilih_empat()" value="Full Aplication Mobile Development" class="btn btn col-5 ml-2 mt-2"> 
                                </div>
                            </form>
                        </div>
                        <script>
                            function pilih_satu (){
                                var form=document.getElementById("satu");
                                if(form.style.background === "white"){
                                    form.style.background="#039BE5";
                                    form.style.color="#FAFAFA";
                                    var status="front_end";
                                    return status;
									}
							    else{
									form.style.background="white";
                                    form.style.color="#424242";
								}
                               
                            }
                            function pilih_dua () {
                                var form=document.getElementById("dua");
                                if(form.style.background === "white"){
                                    form.style.background="#039BE5";
                                    form.style.color="#FAFAFA";
                                    var status="deskop";
                                    return status;
									}
							    else{
									form.style.background="white";
                                    form.style.color="#424242";
								}
                            }
                            function pilih_tiga (){
                                var form=document.getElementById("tiga");
                                if(form.style.background === "white"){
                                    form.style.background="#039BE5";
                                    form.style.color="#FAFAFA";
                                    var status="back_end";
                                    return status;
									}
							    else{
									form.style.background="white";
                                    form.style.color="#424242";
								}
                            }
                            function pilih_empat (){
                                var form=document.getElementById("empat");
                                if(form.style.background === "white"){
                                    form.style.background="#039BE5";
                                    form.style.color="#FAFAFA";
                                    var status="aplication_mobile";
                                    return status;
									}
							    else{
									form.style.background="white";
                                    form.style.color="#424242";
								}
                            }
                            
                            <?php  
                            function kelas_tinggi () {
                                if(isset($_GET['front_end'])){
                                   echo"front_end";  
                                }
                                else if(isset($_GET['deskop'])){
                                    echo "deskop"; 
                                }
                                else if (isset($_GET['back_end'])){
                                    echo "back_end";
                                }
                                else  if (isset($_GET['aplication_mobile'])) {
                                    echo "aplication_mobile"; 
                                }
                            }
                            
                            ?>
                            
                        </script>
                        <div class="selisih-profesional">
                        
                        </div>
                        <div class="tahapawalplus-dua">
                            <h1 class="pl-4 pt-4"><i class="fab fa-bandcamp" style="margin-right:7px;"></i>Kelebihan Dari Setiap Kelas </h1>
                            <p><i class="fas fa-angle-right" style="padding-right:1%;padding-top:10px;"></i>DiDesign Untuk para programmer yang baru belajar</p>
                            <p><i class="fas fa-angle-right" style="padding-right:1%;"></i>Pembelajaran Efektif</p>
                            <p><i class="fas fa-angle-right" style="padding-right:1%;"></i>Modul Modul terpercaya</p>
                            <p><i class="fas fa-angle-right" style="padding-right:1%;"></i>Reward Yang Menarik</p>
                        </div>
                        <div class="selisih-profesional">
                        
                        </div>
                        <div class="tahapawalplus-satu">
                            <h1 class="pl-4 pt-4"><i class="fab fa-bandcamp" style="margin-right:7px;"></i>Biodata Diri</h1>
                            <?php echo validation_errors();
                            echo form_open (base_url('user/registrasi_teamlearn'));?>
                                <input type="hidden" name="pilih_kelas" value="<?php kelas_tinggi();?>">
                                <p>Email</p>
                                <input type="email" name="email" placeholder="enter your email ">
                                <p>Username</p>
                                <input type="text" name="username" placeholder="enter your username ">
                                <p>Password</p>
                                <input type="text" name="password" placeholder="enter your password ">
                                    <center>
                                        <div class="row col-12">
                                            <button type="submit" style=" margin-top:40px; background:#FF7043; margin-left:20%;" class="btn btn text-light col-4" >Daftar Kelas</button>
                                            <a href="<?php  echo base_url('user/cancelregistrasi')?>" style=" margin-top:40px;"  class="btn btn-warning text-light col-2 ml-2 " >Cancel</a>
                                        </div>
                                    </center>
                            </form>
                        </div>   
                </div>
            </div>
        </div>
    </div>
</body>
</html>