<html>
	<head>
		<title>

		</title>
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<script src="http://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/bootstrap/bootstrap.css'); ?>">	
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/index.css') ;?>"> 
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/template.css') ;?>"> 
	</head>
	<body>
		<header>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="<?php  echo base_url('');?>"><img src="<?php  echo base_url('Asset/gambar/logo.png');?>"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
					<div class="logo"><a href="<?php echo base_url(''); ?>"><img src="<?php  echo base_url('Asset/gambar/logo.png');?>"></a></div>
					  <li><a style="color:#37474F;" href="<?php echo base_url('belajar');  ?>"><i class="fas fa-graduation-cap"></i>Belajar</a></li>
					  <li><a  style="color:#37474F;" href="<?php  echo base_url('seminar');  ?>"><i class="far fa-calendar-alt"></i></i>Seminar</a></li>
					  <li class="drop">
						<a href="<?php  echo base_url('lomba');  ?>" class="dropb"><i class="fas fa-trophy"></i>Lomba</a>
						<div class="drop-content">
						  <a href=""><i class="fas fa-key"></i>Masih Berlangsung</a>
						  <a href="#"><i class="fas fa-lock"></i>Sudah Selesai</a>
						</div>
					  </li>
						<li><a style="color:#37474F;" href="<?php  echo base_url('post');  ?>"><i class="fas fa-newspaper"></i>Post</a></li>
					  <li><a style="color:#37474F;" href="<?php  echo base_url('hadiah')?>"><i class="fas fa-gift"></i>Hadiah</a></li>
					<form class="form-inline mt-1 ml-4 col-6">
					  <input style="width:400px;" class="form-control mr-sm-2 ml-2 mt-2 mt-1 left" type="search" placeholder="Search" aria-label="Search">
					  <button class="btn btn-outline-primary  mt-2" type="submit">Search</button>
					</form>
				<div class="login">
					  <li><a href="<?php echo base_url('user/login');?>"><i class="fas fa-sign-in-alt" style="color:white;"></i>Login</a></li>
				</div>
				</ul>
			</div>
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
			</script>
			<script>
			window.onload = function() {
				CKEDITOR.replace( 'editor1' );
			};
		</script>
		<script scr="js/bootstarp/bootstrapt.js"></script>
	</body>
</html>