<html>
	<head>
		<title>

		</title>
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<script src="http://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/bootstrap/bootstrap.css'); ?>">	
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/index.css') ;?>"> 
		<link rel="stylesheet" href="<?php  echo base_url('Asset/css/template.css') ;?>"> 
		<style>
				.dropbtn {
  background-color:  #F5F5F5;
  color: white;
  padding: 12px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
	display: inline-block;
	margin-right:20px;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 60px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
	display: block;

}
.dropdown img {
	width:60px;
	height:60px;
}
.login{
	margin-top:-76px;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover  {background-color: #3e8e41;}
		</style>
	</head>
	<body>
		<header>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="<?php  echo base_url('halamanutama');?>"><img src="<?php  echo base_url('Asset/gambar/logo.png');?>"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
					<div class="logo"><a href="<?php echo base_url('halamanutama'); ?>"><img src="<?php  echo base_url('Asset/gambar/logo.png');?>"></a></div>
					  <li><a style="color:#37474F;" href="<?php echo base_url('halamanutama/belajar');  ?>"><i class="fas fa-graduation-cap"></i>Belajar</a></li>
					  <li><a  style="color:#37474F;" href="<?php  echo base_url('halamanutama/seminar');  ?>"><i class="far fa-calendar-alt"></i></i>Seminar</a></li>
					  <li class="drop">
						<a href="<?php  echo base_url('halamanutama/lomba');  ?>" class="dropb"><i class="fas fa-trophy"></i>Lomba</a>
						<div class="drop-content">
						  <a href=""><i class="fas fa-key"></i>Masih Berlangsung</a>
						  <a href="#"><i class="fas fa-lock"></i>Sudah Selesai</a>
						</div>
					  </li>
						<li><a style="color:#37474F;" href="<?php  echo base_url('halamanutama/post');  ?>"><i class="fas fa-newspaper"></i>Post</a></li>
					  <li><a style="color:#37474F;" href="<?php  echo base_url('halamanutama/hadiah') ?>"><i class="fas fa-gift"></i>Hadiah</a></li>
					<form class="form-inline mt-1 ml-4 col-6">
					  <input style="width:400px;" class="form-control mr-sm-2 ml-2 mt-2 mt-1 left" type="search" placeholder="Search" aria-label="Search">
					  <button class="btn btn-outline-primary  mt-2" type="submit">Search</button>
					</form>
				
				</ul>
			</div>
			<div class="login">
		
				<div class="dropdown">
				<?php  
					if(!empty($template_member)){
						foreach($template_member as $rowf){
			?>
						<img src="<?php  echo base_url('Asset/unggah/'); ?><?php  echo $rowf->gambar;?>" class="dropbtn" >
						<?php  
						}
					}
					else{
						echo"kosong";
					}
				?>
						<div class="dropdown-content">
							<a href="<?php  echo base_url('halamanutama/profil');?>">Profil</a>
							<a href="<?php  echo base_url('user/logout');?>">Logout</a>
				</div>
			
				</div>
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
			</script>
			<script>
			window.onload = function() {
				CKEDITOR.replace( 'editor1' );
			};
		</script>
		<script scr="css/bootstrap/bootstrap.js"></script>
	</body>
</html>