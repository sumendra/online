<html>
	<head>
		<title></title>
	    <link rel="stylesheet" href="<?php  echo base_url('Asset/css/post.css');?>"> 
	<style>
		.footer-container {
			background-image:url("../Asset/gambar/home.jpg");
			background-size:cover;
		}
		.bagian {
			background:#F8F9FA;
			 height:40px; 
		}
		.content-comment{
			margin-left:9%;
		}
		.selisih-comment{
			background:white;
			height:20px;
		}
		input {
			width:80%;
			height:37.9px;
			margin-top:1px;
		}
		/* .comment-left{
			float:left;
			
		}
		.comment-right{
			float:left;
			
		} */
		.comment-semua{
			width:100%;
		}
		.comment-tengah{
			width:100%;
		}
	</style>
	</head>
	<body>
		<div class="jumbotron homeberita" style="background:white;">
	
		<?php 
		if(!empty($post_quer)){
			foreach ($post_quer as $rows){ 
			$tampil=$rows->gambar;
			$hastag=$rows->hastag;
		?>
			<div class="container ">
				<div class="row col-12">
					<div class="row col-sm-4  nav-image">
					  <img src="<?php echo base_url()?>./Asset/gambar/<?php echo $hastag.'.png' ?>">
					</div>
					<div class="row col-sm-4 mt-sm-4">
						<h1 style="font-size:20px;"><?php echo $rows->judul;?></h1>
						<p><?php echo word_limiter ($rows->isi,20);  ?></p>
					</div>
					<div class="row col-sm-2 mt-sm-4">
					</div>
					<div class="row col-3 button-samping">
							<a class="btn btn-success col-6 mr-2" href="<?php  echo base_url('user/login')?>" >Gabung Grup</a>
							<a class="btn btn-danger col-6 mt-2" href="<?php  echo base_url('user/login')?>" ><i  class="fas fa-heart mr-2" id="lihat" onclick="clicklike(this)" style="color:white;"></i></a>
					</div>
				</div>
			</div>
		</div>
		<?php 
			}
		}
		else{
			echo"data kosong";
		}	
		?>
		</div>
		<div class="col-12 bg-light b">
		
		</div>
		<div class="col-12 bg-light">
			<div class="container-fluid bg-light">
				<div class="row col-12">
					<div class="col-7 bg-white content mr-2">
						<img  src="<?php echo base_url()?>./Asset/gambar/<?php echo $tampil; ?>">
                        <h1 class="card-text"><?php echo  $rows->judul;  ?></h1>
						<p class="card-text" style="padding-top:4%;padding-bottom:20px;"><?php echo nl2br ($rows->isi);  ?></p>
						<center>
							<iframe  style="width:420px; height:345px; margin-bottom:10px;" 
								src="<?php  echo $rows->youtube; ?>">
							</iframe>
						</center>
						<p style="text-align:center;padding-top:40px;">Ini video untuk referensi anda</p>
					</div>
					<div class=" row col-3 ml-4">
						<div class="col-12  ">
							<p style="font-size:20px;color:#263238;" class="mt-2">Hubungi Kami</p>
							 <i class="fab fa-facebook-f " style="font-size:30;color:#37474F;"></i>
							 <i class="fab fa-twitter ml-sm-4" style="font-size:30;color:#37474F;"></i>
							 <i class="fab fa-google ml-sm-4" style="font-size:30;color:#37474F"></i>
							 <i class="fab fa-github ml-sm-4" style="font-size:30;color:#37474F;"></i>
							<p style="font-size:20px;color:#263238;" class="pt-3">Recomendasi Artikel </p>
							<li class="pt-sm-1"><a href="<?php echo base_url()?>" class="text-muted">Pengertian dan manfaat HTML dalam pembuatan Website</a></li>
							<li class="pt-sm-2"><a href="<?php echo base_url()?>learning" class="text-muted">Pengertian dan manfaat C# dalam pembuatan Game</a></li>
							<li class="pt-sm-2"><a href="<?php echo base_url()?>events" class="text-muted">Dasar Dasar Belajar PHP</a></li>
							<li class="pt-sm-2"><a href="<?php echo base_url()?>challenge" class="text-muted">Membuat Gui mengguanakan java OOP</a></li>
							<li class="pt-sm-2"><a href="<?php echo base_url()?>login"class="text-muted">Belajar dasar dasar CodeIgniter</a></li>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 bg-light">
				<div class="container-fluid bg-light">
					<div class="col-7 bg-white content-comment mr-2 mt-4 ">
						<?php  
							if(!empty($komentar_semua)){
								foreach($komentar_semua as $rows){
									if(!empty($komentar_amat)){
										foreach($komentar_amat as $rowt ){
									
						?>
						<div class="col-12 row">
							<div class="comment-left">
								<p class="pt-4"><img  style="width:30px;height:30px; margin-right:10%" src="<?php echo base_url('Asset/gambar/user.png');?>"><?php  echo $rows->username?></p> 
								<p class="text-right ml-2"><i><?php echo $rowt->waktu;?></i></p>
							</div>
						</div>
						<div class="comment-tengah">
							<p><?php echo  $rowt->isi ?> </p>
						</div> 
							<?php 
										}
									} 
								}
							}
							else{
								echo"Tidak Ada Commentar";
							}
							?>
						<form>
							<p class="pt-1">Komentar</p>
							<input  type="text" name="comment">
							<a href="<?php  echo base_url('user/login') ?>" class="btn btn-primary text-light col-2 ml-2">Kirim</a>
						</form>
						<div class="col-12 selisih-comment">
	
						</div>
					</div>
				</div>
			</div>
				<script>
					function clicklike (x) {
						x.classList.toggle("fa-thumbs-down");
					}
				</script>
				<div class="col-12 bagian">
		
				</div>	
		</div>		
	<body>
</html>