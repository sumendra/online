<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('/Asset/css/registrasiseminar.css') ?>" />
    <script src="main.js"></script>
    <style>
        .header-registrasi {
			margin-top:-10px;
			background-image:url("../../../Asset/gambar/registrasibasic.jpeg");
			background-size:cover;
        }
        .footer-container {
			margin-top:-10px;
			background-image:url("../../../Asset/gambar/registrasibasic.jpeg");
			background-size:cover;
		}
    </style>
</head>
<body>
<div class="col-12 header-registrasi">
        <div class="container-fluid" style="margin-top:-10px;">
        <div class="col-12" style="height:40px;">
        </div>   
            <div class="row" >
                <div class="penjelasan ">
                    <div class="content-seminar">    
                        <h1>Pendaftaran Lomba</h1>
                        <h2 class="pt-4">Perhatian</h2>
                        <p>1. Isikan kolom username sesuai dengan nama lengkap kalian jika terdapat kesalahan registrasi berkaitan biodata diri  maka itu tanggung jawab dari peserta  </p>  
                        <p>2. Isikan kolom kota asal berdasarkan kota asal lengkap kalian jika terjadi kesalahan maka akan di tanggung oleh peserta   </p>  
                        <p>3. no telp yang di gunakan wajib maasih aktif untuk mempermudah proses  </p> 
                        <p>4. harga sesuia dengan harga yang telah di tetapkan sebelum masuk ke proses registrasi   </p> 
                        <p>5. batas pembayaran merupakan batas dari pembayaran yang harus kalian lakukan setelah menyelesaikan proses registrasi  </p> 
                        <p>6. direkomendasikan untuk tidak mengosongkan kolom    </p> 
                    </div>
                </div>
                <div class="hadiah ml-4">  
                <div class="col-12" style="height:60px;">
                </div> 
                <?php 
                    
                    if(!empty($registrasi_tampil)){
                        foreach($registrasi_tampil as $rowp ){
                            $to=$rowp->id_lomba;
                            $form['id_lomba']=$to;
                ?>
                    <?php  echo validation_errors(); ?>
                    <?php  echo form_open_multipart('halamanutama/lombalomba','',$form) ?>
                        <div class="row">
                            <div class="form-group col-12 ">  
                                    <label>  Harga &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp   <?php  echo $rowp->harga; ?></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12 ">  
                                    <label>  Batas Pembayaran &nbsp <?php  echo $rowp->jadwal; ?> </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12 ">  
                                    <label>  Lokasi &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <?php  echo $rowp->lokasi; ?> </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">  
                                    <label>  Username</label>
                                    <input  type="text" class="form-control col-10 " value="<?php echo $this->session->userdata('nama');?>"  name="nama_lengkap">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">  
                                    <label>  Kota Asal</label>
                                    <input  type="text"  class="form-control col-6 " name="kota_asal" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">  
                                    <label>  No Telp</label>
                                    <input  type="text" class="form-control col-6 " name="no_telp">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">  
                                    <label>  Manfaat Mengikuti Kegiatan </label>
                                    <input  type="text"  class="form-control col-10 " name="manfaat" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">  
                                    <label>  Tujuan Mengikuti Kegiatan</label>
                                    <input  type="text" class="form-control col-10 " name="tujuan">
                            </div>
                        </div>
                        <button type="submit"  class="btn btn-primary">Registrasi</button>
                   </form>
                   <?php  
                        }
                    }
                   ?>
                </div>
            </div>
            <div class="col-12" style="height:30px;">
                
            </div>
        </div>
    </div>
</body>
</html>