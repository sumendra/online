-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2019 at 02:12 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tubes`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(10) NOT NULL,
  `username` int(255) NOT NULL,
  `password` int(255) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `belajar`
--

CREATE TABLE `belajar` (
  `id_belajar` int(10) NOT NULL,
  `judulbelajar` varchar(100) NOT NULL,
  `gambarbelajar` varchar(100) NOT NULL,
  `isibelajar` text NOT NULL,
  `warna` varchar(100) NOT NULL,
  `waktu` int(100) NOT NULL,
  `sponsor` varchar(100) NOT NULL,
  `siswa` int(100) NOT NULL,
  `poin` int(100) NOT NULL,
  `xp` int(100) NOT NULL,
  `tingkat_kelas` varchar(100) NOT NULL,
  `keikutsertaan` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `untuk_jurusan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `belajar`
--

INSERT INTO `belajar` (`id_belajar`, `judulbelajar`, `gambarbelajar`, `isibelajar`, `warna`, `waktu`, `sponsor`, `siswa`, `poin`, `xp`, `tingkat_kelas`, `keikutsertaan`, `harga`, `untuk_jurusan`) VALUES
(1, 'Design Web', 'html.png', 'Pada kelas ini kalian akan di sediakan sekitar 20 modul yang 									nantinya akan mendukung proses pembelajaran kalian. selain									itu kalian  akan di ajarkan HTML  ', '#5C6BC0', 1, 'Samsung', 1000, 100, 400, 'Pemula', 'masih bisa ', 0, 'IF,TK,TT,TI,TE,TF,MI,SI,SK'),
(2, 'Design 2D', 'design2d.png', 'Pada kelas ini kalian akan di beri 20 modul yang nantinya dapat \r\nmembantukalian dalam kalian belajar. kalian juga  berikan\r\npembelajaran berkaitan Adobe ', '#FFA726', 2, 'Microsof', 100, 400, 400, 'Pemula', 'masih bisa', 0, 'IF,MI,MM,SI'),
(3, 'Design 3D', 'blender.png', 'Pada kelas ini kalian akan di berikan sekitar 40 mosul yang nantinya akan\r\n									menunjang proses pembelajaran kalian. kalian juga akan di berikan materi\r\n blender', '#FFCA28', 2, 'Pemerintah', 1200, 400, 100, 'Menengah', 'masih bisa', 0, 'IF,MM,MI'),
(4, 'Aplication Mobile ', 'android.png', 'Pada kelas ini kalian akan di berikan 40 modul sebagai penunjang pembelajaran\r\n									kalian. selain itu kalian juga akan di ajarkan dasar dasar algoritma untuk \r\n					mempermudah pembbuatan program', '#66BB6A', 1, 'Telkom', 100, 400, 400, 'Expert', 'masih bisa', 0, 'TT,IF,MI');

-- --------------------------------------------------------

--
-- Table structure for table `hadiah`
--

CREATE TABLE `hadiah` (
  `id_hadiah` int(10) NOT NULL,
  `judulhadiah` varchar(100) NOT NULL,
  `deskripsihadiah` text NOT NULL,
  `gambarhadiah` varchar(100) NOT NULL,
  `poin_dibutuhkan` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hadiah`
--

INSERT INTO `hadiah` (`id_hadiah`, `judulhadiah`, `deskripsihadiah`, `gambarhadiah`, `poin_dibutuhkan`) VALUES
(1, 'Apple Macbook Pro Tuch Bar', '15.4" Core i7 2.6GHz RAM 16GB Storage 256 GB', 'macbook-pro.jpg', 58000),
(2, 'ASUS ZENBOOK', '14" Core i7 RAM 8 GB Hardisk 128 GB NVIDIA 2G', 'zenbook.jpg', 38000),
(3, 'Samsung Galaxy Note 8', '6.3 RAM 6 GB Storage 64 GB kamera 16 megapix', 'samsung.jpg', 34000),
(4, 'Apple iPhone 6s Plus', '5.5 Storage 32GB kamera 16 mega pixel ram 6 gb', 'iphone6s.jpg', 2000),
(5, 'Samsung Galaxy S8+', '6.2 RAM 4 GB Storage 64 GB kamera 6 megapixel', 'samsungs8.jpg', 24000);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_userfree`
--

CREATE TABLE `kelas_userfree` (
  `id_kelasfree` int(10) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_member` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_userfree`
--

INSERT INTO `kelas_userfree` (`id_kelasfree`, `waktu`, `id_member`) VALUES
(1, '2019-01-07 10:21:30', 73);

-- --------------------------------------------------------

--
-- Table structure for table `kelas_userpremium`
--

CREATE TABLE `kelas_userpremium` (
  `id_kelaspremium` int(10) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status_pembayaran` varchar(100) NOT NULL,
  `id_member` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas_userpremium`
--

INSERT INTO `kelas_userpremium` (`id_kelaspremium`, `nama_kelas`, `waktu`, `status_pembayaran`, `id_member`) VALUES
(20, 'front_end', '2018-12-22 20:08:47', 'belum', 67),
(22, 'front_end', '2018-12-22 20:12:04', 'belum', 71),
(23, 'front_end', '2019-01-14 22:31:51', 'belum', 75),
(24, 'front_end', '2019-01-14 22:36:39', 'belum', 77),
(25, '', '2019-01-14 22:38:53', 'belum', 79),
(26, '', '2019-01-14 22:39:12', 'belum', 81);

-- --------------------------------------------------------

--
-- Table structure for table `komentar_post`
--

CREATE TABLE `komentar_post` (
  `id_komentar` int(10) NOT NULL,
  `isi` text NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_member` int(10) NOT NULL,
  `id_post` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `komentar_post`
--

INSERT INTO `komentar_post` (`id_komentar`, `isi`, `waktu`, `id_member`, `id_post`) VALUES
(32, 'min ini ada bug atau gak ya kodingannya', '2019-01-02 20:09:49', 12, 7),
(33, 'cara exekusi supaya ngak pakek data yang sebelumnya gimana ', '2019-01-02 20:12:07', 12, 7),
(34, 'min biuat kodingan tentang js dong min', '2019-01-02 22:05:11', 12, 7),
(35, 'itu kalau mau buat selain kubus gimana min', '2019-01-06 11:56:00', 12, 1),
(36, 'canvas ini ada di vitur phton gak min', '2019-01-06 12:04:16', 12, 1),
(37, 'thanks minn', '2019-01-06 12:05:08', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id_login` int(10) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_member` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id_login`, `waktu`, `id_member`) VALUES
(57, '2019-01-13 11:55:55', 73),
(58, '2019-01-13 14:28:28', 73),
(59, '2019-01-13 21:24:06', 73),
(60, '2019-01-14 00:24:59', 73),
(61, '2019-01-14 09:57:29', 73),
(62, '2019-01-14 20:11:55', 73),
(63, '2019-01-14 22:46:41', 73),
(64, '2019-01-14 23:32:37', 12),
(65, '2019-01-14 23:34:50', 73),
(66, '2019-01-15 16:51:01', 73),
(67, '2019-01-15 20:01:40', 73);

-- --------------------------------------------------------

--
-- Table structure for table `lomba`
--

CREATE TABLE `lomba` (
  `id_lomba` int(10) NOT NULL,
  `judullomba` varchar(100) NOT NULL,
  `gambarlomba` varchar(100) NOT NULL,
  `isilomba` text NOT NULL,
  `warna` varchar(100) NOT NULL,
  `waktu` int(100) NOT NULL,
  `sponsor` varchar(100) NOT NULL,
  `siswa` int(100) NOT NULL,
  `poin` int(100) NOT NULL,
  `xp` int(100) NOT NULL,
  `tingkat_kelas` varchar(100) NOT NULL,
  `keikutsertaan` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `syarat_lomba` text NOT NULL,
  `jadwal` varchar(900) NOT NULL,
  `lokasi` varchar(900) NOT NULL,
  `susunan_acara` text NOT NULL,
  `faq` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lomba`
--

INSERT INTO `lomba` (`id_lomba`, `judullomba`, `gambarlomba`, `isilomba`, `warna`, `waktu`, `sponsor`, `siswa`, `poin`, `xp`, `tingkat_kelas`, `keikutsertaan`, `harga`, `syarat_lomba`, `jadwal`, `lokasi`, `susunan_acara`, `faq`) VALUES
(1, ' Aplication Development', 'android.png', 'dalam lomba ini kalian akan di berikan suatu tema yang nantinya akan menjadi tema kalian dalam membuata aplikasi.\r\nperlombaan ini akan berlangsung 10 hari ', '#66BB6A', 1, 'Samsung', 1000, 100, 100, 'Menengah', 'masih bisa', 400000, '1.Berumur setidaknya 15 tahun ke atas\r\n2.Mempunyai skill dasar berkaitan jaringan\r\n3.Sudah menyelesaikan proses registrasi pembayaran \r\n4.mengerti dasar dasar aplikasi', '17 February 2018', 'Telkom University', 'Registrasi ulang (07.30 - 08.00) \r\nGeneral Session (08.00 - 12.00) \r\nIshoma (12.00 - 12.30) \r\nproses lomba (12.30 - - 15.00 ) \r\nIshoma (15.00 - 15.30 ) \r\nlomba lanjutan ( 15.30 - 18.00 ) Pembagian Merchandise ( 18.00 - selesa', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop  >> Tidak \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  '),
(2, 'Design UI by Adobe', 'design2d.png', 'bagi kalian yang tertarik kerja di Adobe kalian dapat mengikuti lomba ini, bagi juara satu, dua, dan tiga \r\nakan di rekrut Adobe langsung bekerja di Adobe', '#FFA726', 1, 'Gojek', 1000, 400, 400, 'Pemula', 'masih bisa', 400000, '1.Berumur setidaknya 15 tahun ke atas\r\n2.Mempunyai skill dasar berkaitan jaringan\r\n3.Sudah menyelesaikan proses registrasi pembayaran \r\n4.mengerti dasar dasar design', '18 February 2018', 'Telkom University', 'Registrasi ulang (07.30 - 08.00) \r\nGeneral Session (08.00 - 12.00) \r\nIshoma (12.00 - 12.30) \r\nProses lomba (12.30 - - 15.00 ) \r\nIshoma (15.00 - 15.30 ) \r\nLomba lanjutan  ( 15.30 - 18.00 ) Pembagian Merchandise ( 18.00 -selesai )', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop  >> Tidak \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  '),
(3, 'Web Design', 'html.png', 'Telkom University mengadakan lomba web design .\r\nsistem yang di berikan Telkom University adalah coding langsung di tempat. berminat daftar di kami', '#5C6BC0', 1, 'Telkom', 100, 100, 100, 'Pemula', 'masih bisa', 400000, '1. sudah berumur di atas 15 tahun  \r\n2. sudah menyelesaikan seluruh proses registrasi ', '2 Februari 2018', 'Telkom University', 'Registrasi ulang (07.30 - 08.00) \r\nGeneral Session (08.00 - 12.00) \r\nIshoma (12.00 - 12.30) \r\nProses lomba (12.30 - - 15.00 ) \r\nIshoma (15.00 - 15.30 ) \r\nLomba lanjutan  ( 15.30 - 18.00 ) Pembagian Merchandise ( 18.00 -selesai )', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop  >> Tidak \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  '),
(4, 'Hacking and network Scurity', 'networkscurity.jpg', 'kali ini assosiasi network indonesia mengadakan lomba hacking dan scurity\r\ndan jaringan . \r\nuntuk mencari bibit bibit terkemuka di Indonesia', '#FFCA28', 1, 'Pertamina', 1000, 100, 100, 'Menengah', 'masih bisa', 400000, '1. sudah berumur 15 tahun ke atas \r\n2. sudah menyelesaikan seluruh proses registrasi ', '16 Januari 2018', 'IT Center Bandung', 'registrasi ulang(7.30-9.30) \r\npembukaan perlombaan (9.30-11.00) \r\nproses perlombaan (11.00-14.00) instirahat(14.00-14.30) \r\nlomba lanjutan(14.30-16.00)  \r\npengumuman pemenang terpaforit (16.00-18.00) \r\npengumuman pemenang (18.00-20.00)', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop  >> Tidak \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  ');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(100) NOT NULL,
  `poin` int(100) NOT NULL,
  `xp` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `email`, `username`, `gambar`, `password`, `waktu`, `status`, `poin`, `xp`) VALUES
(12, 'sumendrakadek108@gmail.com', 's', 'user.png', 's', '2018-12-16 17:09:28', 'free', 0, 0),
(71, 'sumendrakadek99@gmail.com', 'sq', 'user.png', 'sq', '2018-12-22 20:12:04', 'profesional', 7300, 7600),
(72, 'sumendrakadek29@gmail.com', 'd', 'user.png', 'd', '2019-01-06 22:36:17', 'free', 0, 0),
(73, 'sumendrakadek44@gmail.com', 'root', 'home.jpg', 'sumendra', '2019-01-07 10:21:30', 'free', 4400, 4400),
(81, 'sumendrakadek62@gmail.com', 'sumendra16', 'user.png', 's', '2019-01-14 22:39:12', 'profesional', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mulai_belajar`
--

CREATE TABLE `mulai_belajar` (
  `id_mulaibelajar` int(10) NOT NULL,
  `id_belajar` int(10) NOT NULL,
  `id_member` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mulai_belajar`
--

INSERT INTO `mulai_belajar` (`id_mulaibelajar`, `id_belajar`, `id_member`) VALUES
(72, 1, 71),
(78, 2, 73),
(79, 4, 73);

-- --------------------------------------------------------

--
-- Table structure for table `mulai_lomba`
--

CREATE TABLE `mulai_lomba` (
  `id_mulailomba` int(10) NOT NULL,
  `tujuan` text NOT NULL,
  `id_member` int(10) NOT NULL,
  `id_lomba` int(10) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `kota_asal` varchar(100) NOT NULL,
  `no_telp` int(100) NOT NULL,
  `manfaat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mulai_lomba`
--

INSERT INTO `mulai_lomba` (`id_mulailomba`, `tujuan`, `id_member`, `id_lomba`, `nama_lengkap`, `kota_asal`, `no_telp`, `manfaat`) VALUES
(32, 'untuk dapat lebih tau tentang html ', 73, 1, 'root', 'bali', 2147483647, 'untuk dapat lebih tau tentang html ');

-- --------------------------------------------------------

--
-- Table structure for table `mulai_seminar`
--

CREATE TABLE `mulai_seminar` (
  `id_mulaiseminar` int(10) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `kota_asal` varchar(100) NOT NULL,
  `no_telp` int(100) NOT NULL,
  `manfaat` text NOT NULL,
  `tujuan` text NOT NULL,
  `id_seminar` int(10) NOT NULL,
  `id_member` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mulai_seminar`
--

INSERT INTO `mulai_seminar` (`id_mulaiseminar`, `nama_lengkap`, `kota_asal`, `no_telp`, `manfaat`, `tujuan`, `id_seminar`, `id_member`) VALUES
(63, 'root', 'bali', 2147483647, 'untuk dapat lebih tau tentang html ', 'untuk dapat lebih tau tentang html ', 1, 73),
(65, 'root', 'bali', 2147483647, 'untuk dapat lebih tau tentang html ', 'untuk dapat lebih tau tentang html ', 1, 73),
(66, 'root', 'bali', 2147483647, 'untuk dapat lebih tau tentang html ', 'untuk dapat lebih tau tentang html ', 1, 73),
(67, 'root', 'bali', 2147483647, 'untuk dapat lebih tau tentang html ', 'untuk dapat lebih tau tentang html ', 1, 73);

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(10) NOT NULL,
  `isi_pesan` varchar(255) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_login` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pilih_hadiah`
--

CREATE TABLE `pilih_hadiah` (
  `id_pilihhadiah` int(10) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `no_telp` int(100) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kota_asal` varchar(100) NOT NULL,
  `id_member` int(10) NOT NULL,
  `id_hadiah` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pilih_hadiah`
--

INSERT INTO `pilih_hadiah` (`id_pilihhadiah`, `nama_lengkap`, `no_telp`, `waktu`, `kota_asal`, `id_member`, `id_hadiah`) VALUES
(1, 'root', 2147483647, '2019-01-13 12:03:40', 'bali', 73, 4);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id_post` int(10) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `warna` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `hastag` varchar(100) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `view` int(100) NOT NULL,
  `suka` int(100) NOT NULL,
  `komentar` int(100) NOT NULL,
  `youtube` varchar(100) NOT NULL,
  `id_admin` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id_post`, `judul`, `isi`, `warna`, `gambar`, `hastag`, `waktu`, `view`, `suka`, `komentar`, `youtube`, `id_admin`) VALUES
(1, 'Penerapan Dalam html Canvas', '    Merupakan suatu fitur dalam html yang telah lama ada seiring dengan meluncurnya html type ke 5. Html type canvas sendiri memiliki beberapa fungsi utama diantaranya :\r\n\r\n    a.Grafik bangunan\r\nArtinya dengan html canvas kita bisa membuat grafik bangunan secara 2d untuk mempercantik tampilana halaman html kalian \r\nb.Animasi \r\nKalian juga bisa menambahkan animasi dengan menggunakan html canvas \r\nc.Game \r\nDengan html canvas digabung javascript kalian dapat membuat game sederhana \r\nd.Komposisi gambar\r\n\r\nB.Contoh HTML Canvas \r\n\r\nUntuk javascript \r\n\r\nVar html_canvas = document.getElementById(“html_canvas”);\r\nVar HTML = canvas.getContext (“2d”);\r\n\r\nKeterangan\r\n\r\n•Width -> digunakan untuk memberi lebar pada suatu canvas\r\n•Height-> digunakan untuk memberi tinggi pada suatu canvas \r\n•Border-> digunakan untuk menambah garis batasan pada  canvas  \r\n•Document.getElementById(“html_canvas”);-> untuk mengambil id pada tag html\r\n•Var-> untuk membuat variable baru dalam javascript  \r\n', '#5C6BC0', 'pengertianhtmlcanvas.jpg', 'html', '2018-12-16 00:48:26', 147, 12, 1, 'https://www.youtube.com/embed/tgbNymZ7vqY?autoplay=1', 1),
(2, 'Pengertian Java interface Object', 'interface merupakan suatu penampung yang digunakan untuk menampung method dalam java', '#ef5350', 'pengertianjavainterface.jpg', 'java', '2018-12-16 00:48:26', 137, 80, 100, '', 1),
(3, 'Apa itu Html Form', 'html form merupakan suatu elemnet dalam html yang berfungsi untuk menginputkan data dari user', '#5C6BC0', 'pengertianhtmlform.png', 'html', '2018-12-16 00:49:06', 137, 72, 100, '', 1),
(4, 'Apa itu Rigging dalam Blender', 'rigging  merupakan suatu teknik dalam blender saat ingin membuat bone', '#FFCA28', 'blender.png', 'blender', '2018-12-16 19:22:29', 138, 33, 100, '', 1),
(5, 'Cara insert Ci', 'ci merupakan suatu framework php yang masih menggunakan cara classic dalam penggunaaanya', '#f44336', 'ci.png', 'ci', '2018-12-16 19:22:29', 136, 42, 100, '', 1),
(6, 'Kegunaan Framework Node js', 'node js merupakan framework berbasis javascript yang digunakan mempercepat pembuatan javascript', '#37474F', 'nodejs.png', 'nodejs', '2018-12-16 19:23:25', 136, 104, 100, '', 1),
(7, 'tuutorial dasar angular js', 'kali ini kita akan membahas sedikit tentang apa  itu angular js', '#ef5350', 'angular.png', 'angular', '2018-12-17 11:16:47', 136, 20, 20, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `seminar`
--

CREATE TABLE `seminar` (
  `id_seminar` int(10) NOT NULL,
  `judulseminar` varchar(100) NOT NULL,
  `gambarseminar` varchar(100) NOT NULL,
  `isiseminar` text NOT NULL,
  `warna` varchar(100) NOT NULL,
  `waktu` int(100) NOT NULL,
  `sponsor` varchar(100) NOT NULL,
  `siswa` int(100) NOT NULL,
  `poin` int(100) NOT NULL,
  `xp` int(100) NOT NULL,
  `tingkat_kelas` varchar(100) NOT NULL,
  `keikutsertaan` varchar(100) NOT NULL,
  `harga` int(100) NOT NULL,
  `syarat_seminar` text NOT NULL,
  `jadwal` varchar(255) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `susunan_acara` text NOT NULL,
  `faq` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seminar`
--

INSERT INTO `seminar` (`id_seminar`, `judulseminar`, `gambarseminar`, `isiseminar`, `warna`, `waktu`, `sponsor`, `siswa`, `poin`, `xp`, `tingkat_kelas`, `keikutsertaan`, `harga`, `syarat_seminar`, `jadwal`, `lokasi`, `susunan_acara`, `faq`) VALUES
(1, 'Aftifical Intelegent', 'aftikalintellegent.jpg', 'pada seminar ini kalian akan di beri tahu apa itu aftifical inttelegent.\r\nkalian juga akan di berikan masukan masukan berkaitan AI', '#1976D2', 1, 'Samsung', 1000, 400, 400, 'Menengah', 'masih bisa', 400000, '1.Berumur setidaknya 15 tahun ke atas\r\n2.Mempunyai skill dasar berkaitan jaringan\r\n3.Sudah menyelesaikan proses registrasi pembayaran\r\n4.mengerti dasar dasar pengertian aftical Intelegent', '6 Maret 2018', 'Telkom University', 'Registrasi ulang (07.30 - 08.00) \r\nGeneral Session (08.00 - 12.00) \r\nIshoma (12.00 - 12.30) \r\nproses seminar (12.30 - - 15.00 ) \r\nIshoma (15.00 - 15.30 ) \r\nseminar lanjutan ( 15.30 - 18.00 ) Pembagian Merchandise ( 18.00 -selesai )', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop  >> Tidak \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  '),
(2, 'Bitcoin Technology', 'bitcoin.gif', 'pada seminar ini kalian akan di beri tahu apa itu bitcoin. \r\nkalian juga akan diberikan masukan masukan cara mudah mencari dan menambang bitcoin', '#6D4C41', 1, 'Microsof', 1000, 2000, 2000, 'Menengah', 'masih bisa', 400000, '1.Berumur setidaknya 15 tahun ke atas \r\n2.Mempunyai skill dasar berkaitan jaringan\r\n3.Sudah menyelesaikan proses registrasi pembayaran', '8 Maret 2018', 'Telkom University', 'Registrasi ulang (07.30 - 08.00) \r\nGeneral Session (08.00 - 12.00) \r\nIshoma (12.00 - 12.30) \r\nParalel Track Network dan Track Games (12.30 - - 15.00 ) \r\nIshoma (15.00 - 15.30 ) \r\nParalel Lanjutan ( 15.30 - 18.00 )\r\nPembagian Merchandise ( 18.00 - selesai )', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop  >> Tidak \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  '),
(3, 'Network Asociation Indonesia', 'networktopologi.jpg', 'pada seminar ini kalian akan di beri tahu apa itu network dan fungsinya.\r\nkalian juga akan di beri masukan masukan cara belajar dan menguasai  network ', '#42A5F5', 1, 'Pemerintah', 200, 2000, 2000, 'Pemula', 'masih bisa', 200000, '1.Berumur setidaknya 15 tahun keatas\r\n2.Mempunyai skill dasar berkaitan jaringan\r\n3.Sudah menyelesaikan proses registrasi pembayaran \r\n4.mengerti dasar dasar pengertian bitcoin', '1 Maret 2018', 'IT Center Bandung', '1.Registrasi ulang (07.30 - 08.00)\r\n2.General Session (08.00 - 12.00)\r\n3.Ishoma (12.00 - 12.30) \r\n4.Paralel Track Bitcoin dan Track Games (12.30 - - 15.00 ) \r\n5.Ishoma (15.00 - 15.30 ) \r\n6.Paralel Lanjutan ( 15.30 - 18.00 )\r\n7.Pembagian Merchandise ( 18.00 - selesai ) 		', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot   \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia   \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan  \r\n4. Apakah saya harus membawa laptop\r\n>> Akami menyarankan anda membawa laptop untuk mempermudah kalian menyerap materi  \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung  '),
(4, 'Hacking and Network scurity', 'networkscurity.jpg', 'pada seminar ini kalian akan di beri tahu apa itu scurity network. \r\nkalian juga akan di berikan masukan masukan cara belajar sehingga dapat menguasai network dengan mudah', '#FFCA28', 1, 'Telkom', 1000, 1000, 1000, 'Menengah', 'masih bisa', 400000, '1.Berumur setidaknya 15 tahun ke atas\r\n2.Mempunyai skill dasar berkaitan jaringan \r\n3.Sudah menyelesaikan proses registrasi pembayaran', '18 January 2018', 'Udayana University', 'Registrasi ulang (07.30 - 08.00) \r\nGeneral Session (08.00 - 12.00) \r\nIshoma (12.00 - 12.30) Paralel Track Network dan Track Games (12.30 - - 15.00 ) \r\nIshoma (15.00 - 15.30 ) \r\nParalel Lanjutan ( 15.30 - 18.00 )\r\nPembagian Merchandise ( 18.00 - selesai )', '1. Apakah saya bisa langsung datang tanpa mendaftar sebelumnya ?  \r\n>> Tidak, tidak ada pendaftaran on the spot  \r\n2. Apabila quota acara ini penuh apakah saya masih bisa ikut ?  \r\n>> Tidak, silahkan waiting list jika masih tersedia  \r\n3. Apakah newbie boleh mengikuti , acara ini ? \r\n>> Boleh , acara ini terbuka untuk siapa saja namun disarankan kalian harus tetap mengikuti syarat syarat yang telah ditentukan \r\n4. Apakah saya harus membawa laptop\r\n>> Akami menyarankan anda membawa laptop untuk mempermudah kalian menyerap materi \r\n5. Apabila saya merupakan sebuah tim yang terdiri dari beberapa orang bagaimana cara saya mendaftar ? \r\n>> silahkan mendaftar secara pribadi dengan jangkauan waktu yang dberdekatan sehingga nomer yang kalian dapat berdekatan  \r\n6. Apa bentuk konfirmasi yang akan kami terima sebagai tiket masuk ke dalam acara  \r\n>> Anda akan menerima email dari kami, beberapa hari sebelum acara berlangsung ');

-- --------------------------------------------------------

--
-- Table structure for table `suka_post`
--

CREATE TABLE `suka_post` (
  `id_suka` int(10) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_member` int(10) NOT NULL,
  `id_post` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `suka_post`
--

INSERT INTO `suka_post` (`id_suka`, `waktu`, `id_member`, `id_post`) VALUES
(2, '2019-01-06 11:21:03', 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `view_post`
--

CREATE TABLE `view_post` (
  `id_view` int(10) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_member` int(10) NOT NULL,
  `id_post` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `view_post`
--

INSERT INTO `view_post` (`id_view`, `waktu`, `id_member`, `id_post`) VALUES
(16, '2019-01-06 11:00:24', 12, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `belajar`
--
ALTER TABLE `belajar`
  ADD PRIMARY KEY (`id_belajar`);

--
-- Indexes for table `hadiah`
--
ALTER TABLE `hadiah`
  ADD PRIMARY KEY (`id_hadiah`);

--
-- Indexes for table `kelas_userfree`
--
ALTER TABLE `kelas_userfree`
  ADD PRIMARY KEY (`id_kelasfree`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `kelas_userpremium`
--
ALTER TABLE `kelas_userpremium`
  ADD PRIMARY KEY (`id_kelaspremium`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `komentar_post`
--
ALTER TABLE `komentar_post`
  ADD PRIMARY KEY (`id_komentar`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_post` (`id_post`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id_login`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `lomba`
--
ALTER TABLE `lomba`
  ADD PRIMARY KEY (`id_lomba`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `mulai_belajar`
--
ALTER TABLE `mulai_belajar`
  ADD PRIMARY KEY (`id_mulaibelajar`),
  ADD KEY `id_belajar` (`id_belajar`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `mulai_lomba`
--
ALTER TABLE `mulai_lomba`
  ADD PRIMARY KEY (`id_mulailomba`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_lomba` (`id_lomba`);

--
-- Indexes for table `mulai_seminar`
--
ALTER TABLE `mulai_seminar`
  ADD PRIMARY KEY (`id_mulaiseminar`),
  ADD KEY `id_seminar` (`id_seminar`),
  ADD KEY `id_member` (`id_member`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`),
  ADD KEY `id_login` (`id_login`);

--
-- Indexes for table `pilih_hadiah`
--
ALTER TABLE `pilih_hadiah`
  ADD PRIMARY KEY (`id_pilihhadiah`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_hadiah` (`id_hadiah`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `id_admin` (`id_admin`);

--
-- Indexes for table `seminar`
--
ALTER TABLE `seminar`
  ADD PRIMARY KEY (`id_seminar`);

--
-- Indexes for table `suka_post`
--
ALTER TABLE `suka_post`
  ADD PRIMARY KEY (`id_suka`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_post` (`id_post`);

--
-- Indexes for table `view_post`
--
ALTER TABLE `view_post`
  ADD PRIMARY KEY (`id_view`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_post` (`id_post`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `belajar`
--
ALTER TABLE `belajar`
  MODIFY `id_belajar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hadiah`
--
ALTER TABLE `hadiah`
  MODIFY `id_hadiah` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kelas_userfree`
--
ALTER TABLE `kelas_userfree`
  MODIFY `id_kelasfree` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kelas_userpremium`
--
ALTER TABLE `kelas_userpremium`
  MODIFY `id_kelaspremium` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `komentar_post`
--
ALTER TABLE `komentar_post`
  MODIFY `id_komentar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id_login` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `lomba`
--
ALTER TABLE `lomba`
  MODIFY `id_lomba` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `mulai_belajar`
--
ALTER TABLE `mulai_belajar`
  MODIFY `id_mulaibelajar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `mulai_lomba`
--
ALTER TABLE `mulai_lomba`
  MODIFY `id_mulailomba` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `mulai_seminar`
--
ALTER TABLE `mulai_seminar`
  MODIFY `id_mulaiseminar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pilih_hadiah`
--
ALTER TABLE `pilih_hadiah`
  MODIFY `id_pilihhadiah` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `seminar`
--
ALTER TABLE `seminar`
  MODIFY `id_seminar` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suka_post`
--
ALTER TABLE `suka_post`
  MODIFY `id_suka` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `view_post`
--
ALTER TABLE `view_post`
  MODIFY `id_view` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
